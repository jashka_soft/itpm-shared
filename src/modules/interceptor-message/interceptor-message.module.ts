import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { MessageHttpInterceptor } from "./interceptor-message";

@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MessageHttpInterceptor,
      multi: true
    }
  ]
})
export class InterceptorMessageModule {}