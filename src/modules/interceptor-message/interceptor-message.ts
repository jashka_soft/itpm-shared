import { Injectable } from "@angular/core";
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from "@angular/common/http";

import { Observable } from "rxjs/Rx";
import "rxjs/add/observable/throw";
import "rxjs/add/operator/catch";

import { ToastsManager } from "ng2-toastr";

@Injectable()
export class MessageHttpInterceptor implements HttpInterceptor {
  constructor(private ToastsManager: ToastsManager) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.body.hasOwnProperty('message')) {
            this.ToastsManager.success(event.body.message);
          }
        }

        return event;
      })
      .catch((error: HttpErrorResponse) => {
        switch (error.status) {
          case 401:
          case 422:
          case 403:
            this.ToastsManager.error(error.error.message);
            break;
          case 404:
            this.ToastsManager.error('Ресурс не знайдений');
            break;
          default:
            this.ToastsManager.error('Server error');
        }

        return Observable.throw(error);
      });
  }
}