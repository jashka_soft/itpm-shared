import { Directive, ElementRef, HostListener, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";

import { NamedRouteService } from "../services/named-route.service";

@Directive({
  selector: '[namedRoute]'
})
export class NamedRouteDirective implements OnInit {

  @Input() namedRoute: string;
  @Input() namedRouteParams: any;

  @HostListener('click', ['$event']) onClick($event: Event) {
    $event.preventDefault();

    this.Router.navigateByUrl(this.url);
  }

  private url: string;

  constructor(private ElementRef: ElementRef,
              private Router: Router,
              private NamedRouteService: NamedRouteService) {
  }

  public ngOnInit() {
    let params = this.namedRouteParams ? this.namedRouteParams : {};
    this.url = this.NamedRouteService.getRouteUrl(this.namedRoute, params);
    this.ElementRef.nativeElement.href = this.url;
  }
}