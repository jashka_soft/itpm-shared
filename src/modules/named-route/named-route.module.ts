import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { NamedRouteDirective } from "./directives/named-route.directive";
import { NamedRouteService } from "./services/named-route.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    NamedRouteDirective
  ],
  providers: [
    NamedRouteService
  ],
  exports: [
    NamedRouteDirective
  ]
})
export class NamedRouteModule {
}