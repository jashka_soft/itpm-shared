import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { TableResultsComponent } from "./table-results/table-results.component";
import { TableResultsService } from "./table-results/services/table-results.service";
import { TableItemComponent } from "./table-results/table-item/table-item.component";
import { TabPipe } from "./table-results/table-item/pipes/tab.pipe";
import { StatusPipe } from "./table-results/table-item/pipes/status.pipe";
import { ResultComponent } from "./table-results/result/result.component";
import { ResultService } from "./table-results/result/services/result.service";
import { TableResultsMainComponent } from './table-results/table-results-main/table-results-main.component';
import { PaginationModule } from '../pagination/pagination.module';
import { AuthModule } from '../auth/auth.module';
import { AppConfig, AppEnv, ITPMModule } from '../itpm/itpm.module';

const routes: Routes = [
  {
    path: '',
    component: TableResultsComponent,
  },
  {
    path: ':id',
    component: ResultComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(routes),
    NgbModule,

    PaginationModule,
    AuthModule,
    ITPMModule
  ],
  providers: [
    TableResultsService,
    ResultService
  ],
  declarations: [
    TableResultsComponent,
    ResultComponent,
    TableItemComponent,
    TableResultsMainComponent,

    TabPipe,
    StatusPipe
  ]
})
export class StatisticModule {
  public static forRoot(config: AppConfig): ModuleWithProviders {
    return {
      ngModule: ITPMModule,
      providers: [
        {
          provide: AppEnv,
          useValue: config
        },
      ]
    }
  }

}
