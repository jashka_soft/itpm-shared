import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ResultService } from "./services/result.service";
import { Result } from "./models/result";
import { DiagramInstanceConfig, instances } from '../../../diagram/instances';
import { Permissions } from '../../../diagram/security/permission/permissions';

@Component({
  selector: 'app-result',
  templateUrl: `./result.component.html`,
})
export class ResultComponent implements OnInit {

  public result: Result;

  private config: any = {};
  private types: DiagramInstanceConfig[] = [];

  constructor(private ActivatedRoute: ActivatedRoute,
              private ResultService: ResultService) {
    this.types = instances;
    this.config.role = 'user';
    this.config.permission = Permissions.ONLY_READ;
    this.config.controls = false;
    this.config.showBtnSave = false;
  }

  public ngOnInit() {
    const id: number = +this.ActivatedRoute.snapshot.params['id'];
    this.ResultService.getResult(id).subscribe(data => {
      this.result = data;
    });
  }
}
