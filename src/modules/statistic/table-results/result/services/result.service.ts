import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import { map } from 'rxjs/operators';

import { QuestionResult, Result } from "../models/result";
import { AppHttpClient } from '../../../../itpm/services/app-http-client';
import { rc4 } from '../../../../diagram/security/rc4';

@Injectable()
export class ResultService {

  private api: string;

  constructor(private HttpClient: AppHttpClient) {
    this.api = `statistics`;
  }

  public getResult(id: number): Observable<Result> {
    return this
      .HttpClient
      .withoutPrefixUrl()
      .get<Result>(`${this.api}/${id}`)
      .pipe(
        map((result: Result) => {
          result.test = this.fillRightAnswers(result.test);
          result.schema.diagram = this.decodeSchemaData(result.user_id, result.schema.diagram);

          return result;
        })
      );
  }

  private fillRightAnswers(questions: Array<QuestionResult>): Array<QuestionResult> {
    return questions.map((question: QuestionResult) => {
      question.is_right = question.correct_answer.id === question.answer.id;
      return question;
    });
  }

  private decodeSchemaData(userId: number, schema: string): any {
    const res = rc4(userId.toString(), schema);

    return JSON.parse(res);
  }
}
