import { Answer } from '../../../../itpm/models/test/answer';
import { Question } from '../../../../itpm/models/test/question';
import { Unit } from '../../../../itpm/models/unit';
import { User } from '../../../../auth/model/user';

export interface ISchema {
  created_at: string;
  id: number;
  type: string;
  diagram: string | any;
  examination_info_id: number;
  schema_id: number;
  updated_at: string;
}

export interface QuestionResult {
  answer: Answer;
  answer_id: number;
  correct_answer: Answer;
  correct_answer_id: number;
  examination_info_id: number;
  id: number;
  question: Question;
  question_id: number;
  is_right?: boolean;
}

export interface Result {
  current_tab: number;
  id: number;
  schema: ISchema;
  status: number;
  test: Array<QuestionResult>;
  unit: Unit;
  unit_id: number;
  user: User;
  user_id: number;
  uuid: string;
  created_at: string;
  updated_at: string;
  deleted_at?: string;
}