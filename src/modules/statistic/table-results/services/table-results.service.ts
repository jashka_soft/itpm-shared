import { Injectable } from "@angular/core";
import { HttpParams } from '@angular/common/http';

import { Observable } from "rxjs/Observable";

import { Result } from '../result/models/result';
import { AppHttpClient } from '../../../itpm/services/app-http-client';
import { IPagination } from '../../../pagination/pagination/model/pagination';

export const ALL: boolean = false;
export const SOFT_DELETE: boolean = true;

@Injectable()
export class TableResultsService {

  private api: string;
  private params: Object | any = {};

  constructor(private HttpClient: AppHttpClient) {
    this.api = `statistics`;
  }

  protected checkParams() {
    let fromObject: any = {};
    for (let key in this.params) {
      if (this.params.hasOwnProperty(key) && this.params[key]) {
        fromObject[key] = this.params[key];
      }
    }

    return new HttpParams({ fromObject }).toString();
  }

  public getStatistics(): Observable<IPagination<Result>> {
    return this
      .HttpClient
      .withoutPrefixUrl()
      .get<IPagination<any>>(`${this.api}?${this.checkParams()}`);
  }

  public byUser(id: number) {
    this.params['user_id'] = id;

    return this;
  }

  public byUnit(id: number) {
    this.params['unit_id'] = id;

    return this;
  }

  public bySearch(search: string) {
    this.params['search'] = search;

    return this;
  }

  public byTab(tab: number) {
    this.params['current_tab'] = tab;

    return this;
  }

  public byStatus(status: number) {
    this.params['status'] = status;

    return this;
  }

  public byType(softDelete: boolean) {
    this.params['soft_delete'] = softDelete;

    return this;
  }

  public clearParams() {
    this.params = {};

    return this;
  }

  public softDelete(id: number): Observable<Result> {
    return this.HttpClient
      .withoutPrefixUrl()
      .post<Result>(`${this.api}/soft-delete/${id.toString()}`, {});
  }
}
