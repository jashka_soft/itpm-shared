import { Component, OnInit } from "@angular/core";

import { Subject } from "rxjs/Subject";

import { ALL, SOFT_DELETE, TableResultsService } from "./services/table-results.service";
import { IStatus, statuses, tabs } from "./table-item/statuses";

import { Result } from './result/models/result';
import { AuthService } from '../../auth/services/auth.service';
import { PaginationService } from '../../pagination/pagination/service/pagination.service';
import { Unit } from '../../itpm/models/unit';
import { User } from '../../auth/model/user';
import { IPagination } from '../../pagination/pagination/model/pagination';

@Component({
  selector: 'app-table-results',
  templateUrl: './table-results.component.html',
})
export class TableResultsComponent implements OnInit {

  public tabs: Array<IStatus>;
  public statuses: any;
  public search: string;
  public searchSubject: Subject<string> = new Subject<string>();
  public isAdmin: boolean = false;

  constructor(private TableResultsService: TableResultsService,
              private AuthService: AuthService,
              public statistics: PaginationService<Result>) {
    this.isAdmin = this.AuthService.getUser().role === 'admin';
    this.tabs = Object.keys(tabs).map((key) => {
      return {id: +key, text: tabs[key]};
    });
    this.statuses = Object.keys(statuses).map((key) => {
      return {id: +key, text: statuses[key]};
    });
  }

  public ngOnInit() {
    this.TableResultsService.getStatistics().subscribe(pagination => {
      this.statistics.update(pagination);

      this.searchSubject
        .debounceTime(500)
        .distinctUntilChanged()
        .subscribe(model => {
          this.TableResultsService.bySearch(model).getStatistics().subscribe(this.update.bind(this));
        });
    });
  }

  public onSearchChange(search: string) {
    this.searchSubject.next(search);
  }

  public filterByTab(tab: number) {
    this.TableResultsService.byTab(tab).getStatistics().subscribe(this.update.bind(this));
  }

  public filterByStatus(status: number) {
    this.TableResultsService.byStatus(status).getStatistics().subscribe(this.update.bind(this));
  }

  public filterByUnit(unit: Unit) {
    this.TableResultsService.byUnit(unit.id).getStatistics().subscribe(this.update.bind(this));
  }

  public filterByUser(user: User) {
    this.TableResultsService.byUser(user.id).getStatistics().subscribe(this.update.bind(this));
  }

  public all() {
    this.TableResultsService.byType(ALL).getStatistics().subscribe(this.update.bind(this));
  }

  public bySoftDelete() {
    this.TableResultsService.byType(SOFT_DELETE).getStatistics().subscribe(this.update.bind(this));
  }

  public softDelete(id: number) {
    this.TableResultsService.softDelete(id).subscribe((result: Result) => {
      const stat = this.statistics.getFromCollection(id);
      stat.deleted_at = new Date().toISOString();
      this.statistics.replaceInCollection(id, stat);
    });
  }

  public clearFilters() {
    this.search = '';
    this.searchSubject.next(this.search);
    this.TableResultsService.clearParams().getStatistics().subscribe(this.update.bind(this));
  }

  protected update(pagination: IPagination<Result>) {
    this.statistics.update(pagination);
  }

}
