import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";

import { statuses, tabs } from "./statuses";
import { Result } from '../result/models/result';
import { User } from '../../../auth/model/user';
import { Unit } from '../../../itpm/models/unit';
import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: `[table-item]`,
  templateUrl: `./table-item.component.html`,
  styleUrls: [
    `./table-item.component.css`
  ]
})
export class TableItemComponent implements OnInit {

  @Input() item: Result;
  @Output() onClickUser = new EventEmitter<User>();
  @Output() onClickUnit = new EventEmitter<Unit>();
  @Output() onSoftDelete = new EventEmitter<number>();

  public isAdmin: boolean = false;
  public isEnd: boolean = false;

  private tabs: Object;
  private statuses: Object;

  constructor(private AuthService: AuthService) {
    this.isAdmin = this.AuthService.getUser().role === 'admin';
    this.tabs = tabs;
    this.statuses = statuses;
  }

  public ngOnInit () {
    this.isEnd = this.item.status === 2;
  }

  public byUser() {
    this.onClickUser.emit(this.item.user);
  }

  public byUnit() {
    this.onClickUnit.emit(this.item.unit);
  }

  public softDelete() {
    this.onSoftDelete.emit(this.item.id);
  }

}