export const tabs: any = {
  1: 'Створений',
  2: 'Теорія',
  3: 'Практика',
  4: 'Тест'
};

export const statuses: any = {
  1: 'Проходить',
  2: 'Завершений'
};

export interface IStatus {
  id: number;
  text: string
}