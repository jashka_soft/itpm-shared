import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AuthService } from "./services/auth.service";
import { JWTService } from "./services/jwt.service";
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    AuthService,
    JWTService,
    AuthGuard
  ],
  exports: []
})
export class AuthModule {

}