import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { AppHttpClient } from "../../itpm/services/app-http-client";
import { StorageService } from "../../itpm/services/storage.service";
import { JWTService } from "./jwt.service";
import { User } from "../model/user";

@Injectable()
export class AuthService {

  constructor(private HttpClient: AppHttpClient,
              private JWTService: JWTService,
              private StorageService: StorageService) {
  }

  public isAuth(): boolean {
    return this.JWTService.isAuthenticated();
  }

  public getUser(): User {
    return this.StorageService.get('user');
  }

  public logout() {
    this.StorageService.rm('user');
    this.JWTService.removeToken();
  }

  private onAuth (data: any): any {
    this.StorageService.set('user', data.user);
    this.JWTService.setToken(data.token);

    return data;
  }

  public create(user:User) {
    return this.HttpClient
      .withoutPrefixUrl()
      .post(`auth/signup`, user );
  }

  public auth(user: User): Observable<any> {
    return this.HttpClient
      .withoutPrefixUrl()
      .post(`auth/signin`, user)
      .map(this.onAuth.bind(this));
  }

}