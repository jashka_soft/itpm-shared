import { Injectable } from "@angular/core";

import { StorageService } from "../../itpm/services/storage.service";

@Injectable()
export class JWTService {

  private tokenName: string = 'auth_token';

  constructor(private storage: StorageService) {
  }

  public parse(token: string): any {
    let base64Url = token.split('.')[1];
    let base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

  public setToken(token: string): JWTService {
    this.storage.set(this.tokenName, token);

    return this;
  }

  public isAuthenticated(): boolean {
    return this.isExpired();
  }

  public isExpired(): boolean {
    let token = this.getToken();
    if (token) {
      let params = this.parse(token);
      let isExpired = Math.round(new Date().getTime() / 1000) <= params.exp;

      if (!isExpired) {
        if (this.storage.get('user') || this.storage.get(this.tokenName)) {
          this.storage.rm('user').rm(this.tokenName);
        }
      }

      return isExpired;
    } else {
      return false;
    }
  }

  public removeToken(): JWTService {
    this.storage.rm(this.tokenName);

    return this;
  }

  public getToken(): string {
    return this.storage.get(this.tokenName);
  }
}