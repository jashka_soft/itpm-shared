import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { StorageService } from "./services/storage.service";
import { AppHttpClient, applicationHttpClientFactory } from "./services/app-http-client";
import { JWTService } from "../auth/services/jwt.service";
import { SchemaComponent } from "./components/schema/schema.component";
import { SchemaService } from "./components/schema/schema.service";
import { SliderComponent } from './components/slider/slider.component';
import { ModalConfirmDeactivateService } from './components/schema/modal-confirm-deactivate/modal-confirm-deactivate.service';
import { DiagramSavedService } from './components/schema/diagram-saved.service';
import { ModalConfirmDeactivateComponent } from './components/schema/modal-confirm-deactivate/modal-confirm-deactivate.component';
import { DiagramNonSavedGuard } from './components/schema/diagram-non-saved.guard';

export interface AppConfig {
  apiHost: string;
  apiPrefix: string;
  adminPrefix: string;
  production: boolean;
  attachments: string;
}

export class AppEnv implements AppConfig {
  attachments: string;
  apiHost: string;
  apiPrefix: string;
  adminPrefix: string;
  production: boolean;
}

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,

    NgbModule
  ],
  providers: [
    StorageService,
    SchemaService,
    ModalConfirmDeactivateService,
    DiagramSavedService,
    DiagramNonSavedGuard,
  ],
  declarations: [
    SchemaComponent,
    SliderComponent,
    ModalConfirmDeactivateComponent,
  ],
  exports: [
    SchemaComponent,
    SliderComponent,
    ModalConfirmDeactivateComponent,
  ],
  entryComponents:[
    ModalConfirmDeactivateComponent
  ]
})
export class ITPMModule {
  public static forRoot(config: AppConfig): ModuleWithProviders {
    return {
      ngModule: ITPMModule,
      providers: [
        {
          provide: AppEnv,
          useValue: config
        },
        {
          provide: AppHttpClient,
          useFactory: applicationHttpClientFactory,
          deps: [HttpClient, JWTService, AppEnv]
        },
      ]
    }
  }
}