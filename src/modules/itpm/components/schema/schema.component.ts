import {
  Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output,
  ViewChild, ViewEncapsulation
} from '@angular/core';

import { Subscription } from 'rxjs/Subscription';
import 'rxjs/operator/map';

import { SchemaService } from './schema.service';
import { TypeExport } from '../../../diagram/diagram.interface';
import { DiagramInstanceConfig } from '../../../diagram/instances';
import { ConfigSchemaInterface } from './config-schema.interface';
import { Schema } from './schema';
import { Diagram } from '../../../diagram/types/diagram';
import { Permissions } from '../../../diagram/security/permission/permissions';

export function deepCopy(object: any): any {
  return JSON.parse( JSON.stringify(object) );
}

@Component({
  selector: `schema`,
  templateUrl: `./schema.component.html`,
  styleUrls: [`./schema.component.css`],
  encapsulation: ViewEncapsulation.None,
})
export class SchemaComponent implements OnInit, OnDestroy {
  @Input() schema: Schema;

  // config diagram
  @Input() set config(config: ConfigSchemaInterface) {
    this._config = { ...this._config, ...config };
  };

  get config() {
    return this._config;
  }

  // diagram classes configuration
  @Input() instances: DiagramInstanceConfig[] = [];


  // on save schema
  @Output() onSave: EventEmitter<Schema> = new EventEmitter<Schema>();

  @Output() onUpdateModel: EventEmitter<Schema> = new EventEmitter<Schema>();

  @Output() onInit: EventEmitter<Diagram> = new EventEmitter<Diagram>();


  // gojs block
  @ViewChild('goJSDiagram') diagramElement: ElementRef;

  // fullscreen block
  @ViewChild('fullscreenContainer') fullScreenElement: ElementRef;

  @ViewChild('controlsContainer') controlsElement: ElementRef;

  public exportTypes: typeof TypeExport;
  public isCollapsed = true;
  public subModel: Subscription;

  private _config: ConfigSchemaInterface = {
    permission: Permissions.FULL_ACCESS,
    role: '',
    controls: true,
    schema: this.schema,
    showBtnSave: true,
  };

  constructor(private SchemaService: SchemaService) {
    this.exportTypes = TypeExport;
  }

  public ngOnDestroy() {
    this.SchemaService.destroyDiagram();
    this.subModel.unsubscribe();
  }

  public ngOnInit() {
    this.SchemaService.initFullscreen(this.fullScreenElement);
    this.SchemaService.setupConfiguration(
      this.config,
      this.diagramElement,
      this.controlsElement
    );
    this.onInit.emit();
    this.subModel = this.SchemaService.initDiagram(deepCopy(this.schema)).subscribe((model: Schema) => {
      if (this.config.permission !== Permissions.ONLY_READ) {
        this.updateModel(model);
      }
    });
  }

  public fullscreen() {
    this.SchemaService.toggleFullscreen();
  }

  public exportDiagram(type: TypeExport) {
    this.SchemaService.exportDiagram(type);
  }

  public save() {
    const schema = this.SchemaService.getDiagramModel();
    schema.name = this.schema.name;
    this.onSave.emit(deepCopy(schema));
  }

  public onSelectType() {
    this.SchemaService.destroyDiagram();
    this.subModel.unsubscribe();
    this.subModel = this.SchemaService.initDiagram(this.schema, 'destroy').subscribe(model => {
      if (this.config.permission !== Permissions.ONLY_READ) {
        this.updateModel(model);
      }
    });
  }

  private updateModel(schema: Schema): void {
    schema.name = this.schema.name;
    this.onUpdateModel.emit(deepCopy(schema));
  }
}