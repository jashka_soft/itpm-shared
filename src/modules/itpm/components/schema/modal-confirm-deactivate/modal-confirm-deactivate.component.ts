import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmDeactivateService } from './modal-confirm-deactivate.service';

@Component({
  templateUrl: `./modal-confirm-deactivate.component.html`,
})
export class ModalConfirmDeactivateComponent {
  constructor(private activeModal: NgbActiveModal,
              private ModalConfirmDeactivateService: ModalConfirmDeactivateService) {}

  public dismiss() {
    this.activeModal.dismiss('cross click');
    this.ModalConfirmDeactivateService.cancel();
  }

  public working() {
    this.activeModal.close('close click');
    this.ModalConfirmDeactivateService.cancel();
  }

  public dontSave() {
    this.ModalConfirmDeactivateService.confirm();
    this.activeModal.close('dont save');
  }
}