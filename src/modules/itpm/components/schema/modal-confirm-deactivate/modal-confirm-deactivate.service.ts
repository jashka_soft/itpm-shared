import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ModalConfirmDeactivateService {
  private confirm$: Subject<boolean> = new Subject<boolean>();

  public confirm() {
    this.confirm$.next(true);
  }

  public cancel() {
    this.confirm$.next(false);
  }

  public getConfirmation(): Observable<boolean> {
    return this.confirm$.asObservable();
  }
}