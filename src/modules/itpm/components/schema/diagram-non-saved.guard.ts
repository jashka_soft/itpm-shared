import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';

import { Observable } from "rxjs/Observable";

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { DiagramSavedService } from './diagram-saved.service';
import { ModalConfirmDeactivateComponent } from './modal-confirm-deactivate/modal-confirm-deactivate.component';
import { ModalConfirmDeactivateService } from './modal-confirm-deactivate/modal-confirm-deactivate.service';

@Injectable()
export class DiagramNonSavedGuard<PracticePageComponent> implements CanDeactivate<PracticePageComponent> {
  public constructor(private DiagramSavedService: DiagramSavedService,
                     private ModalConfirmDeactivateService: ModalConfirmDeactivateService,
                     private NgbModal: NgbModal) {}

  public canDeactivate(component: PracticePageComponent,
                       currentRoute: ActivatedRouteSnapshot,
                       currentState: RouterStateSnapshot,
                       nextState?: RouterStateSnapshot,): Observable<boolean> | Promise<boolean> | boolean {
    if (this.DiagramSavedService.isSave()) {
      return this.DiagramSavedService.isSave();
    }

    this.NgbModal.open(ModalConfirmDeactivateComponent);
    return this.ModalConfirmDeactivateService
      .getConfirmation()
      .switchMap((result: boolean) => Observable.of(result));
  }

}