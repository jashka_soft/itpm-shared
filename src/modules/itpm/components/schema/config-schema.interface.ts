import { Schema } from './schema';
import { Permissions } from '../../../diagram/security/permission/permissions';

export interface ConfigSchemaInterface {
  permission: Permissions;
  role: string;
  controls: boolean;
  showBtnSave: boolean;
  schema: Schema;
}