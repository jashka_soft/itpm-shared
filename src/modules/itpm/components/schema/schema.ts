import { Link as GoLink, Node } from 'gojs';

import { Task } from '../../../diagram/types/gantt/models/task';
import { Link } from '../../../diagram/types/gantt/models/link';

export interface DiagramDataInterface {
  'class': string;
  nodeDataArray: Array<Node> | null;
  linkDataArray: Array<GoLink> | null;
}

export interface WBSDiagramDataInterface extends DiagramDataInterface {
  palette: Array<Node>;
}

export class Schema {
  id: number;
  name: string = 'Schema name';
  type: string = 'WBSOBS';
  unit_id: number;
  palette?: Array<Object> = [];
  diagram: any = {};

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

export class GoSchema extends Schema {
  diagram: DiagramDataInterface = {
    'class': 'go.TreeModel',
    nodeDataArray: [],
    linkDataArray: [],
  }
}

export class WBSSchema extends GoSchema {
  diagram: WBSDiagramDataInterface = {
    'class': 'go.TreeModel',
    nodeDataArray: [],
    linkDataArray: [],
    palette: []
  };
}

export class GanttSchema extends Schema {
  diagram: {tasks: Array<Task>, links: Array<Link>} = {tasks: [], links: []};
}