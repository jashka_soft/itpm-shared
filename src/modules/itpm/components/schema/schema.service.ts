import { ElementRef, Injectable, NgZone } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/operator/map';

import { AppHttpClient } from '../../services/app-http-client';
import { Diagram } from '../../../diagram/types/diagram';
import { DiagramFactory, DiagramInstanceConfig, instances } from '../../../diagram/instances';
import { ConfigSchemaInterface } from './config-schema.interface';
import { Schema } from './schema';
import { TypeExport } from '../../../diagram/diagram.interface';

@Injectable()
export class SchemaService {
  private prefix: string;
  private element: any;
  private requestFullscreen: any;
  private isFullscreen: boolean = false;
  private diagram: Diagram;
  private config: ConfigSchemaInterface;
  private schema: Schema = new Schema();
  private diagramElement: ElementRef;
  private controlsElement: ElementRef;

  constructor(private AppHttpClient: AppHttpClient,
              private NgZone: NgZone) {
    this.prefix = 'schema/';
  }

  public setupConfiguration(
    config: ConfigSchemaInterface,
    diagramElement: ElementRef,
    controlsElement: ElementRef,
  ) {
    this.config = config;
    this.diagramElement = diagramElement;
    this.controlsElement = controlsElement;
  }

  public initDiagram(schema: Schema, destroy?: string): Observable<any> {
    this.schema = destroy ? this.makeNewSchema(schema) : schema;
    this.config.schema = this.schema;
    this.diagram = this.makeDiagram();
    this.diagram.onUpdateModel().subscribe((model: Schema) => {
      this.schema = model;
    });

    this.diagram.initDiagram(this.schema);

    return this.diagram.onUpdateModel();
  }

  public getDiagramModel() {
    return this.schema;
  }

  public destroyDiagram(): SchemaService {
    this.diagram.destroy();

    return this;
  }

  public exportDiagram(type: TypeExport): SchemaService {
    this.diagram.export(type, this.schema);

    return this;
  }

  public initFullscreen(element: ElementRef): SchemaService {
    this.element = element.nativeElement;

    if (this.element.requestFullscreen) {
      this.requestFullscreen = this.element.requestFullscreen;
    } else if (this.element.msRequestFullscreen) {
      this.requestFullscreen = this.element.msRequestFullscreen;
    } else if (this.element.mozRequestFullScreen) {
      this.requestFullscreen = this.element.mozRequestFullScreen;
    } else if (this.element.webkitRequestFullscreen) {
      this.requestFullscreen = this.element.webkitRequestFullscreen;
    }

    return this;
  }

  public toggleFullscreen(): SchemaService {
    if (this.isFullscreen) {
      this.closeFullscreen();
      this.isFullscreen = false;
    } else {
      this.requestFullscreen.call(this.element);
      this.isFullscreen = true;
    }

    return this;
  }

  public get(id: number): Observable<Schema> {
    return this.AppHttpClient
      .withoutPrefixUrl()
      .get<Schema>(`${this.prefix}${id}`);
  }

  public store(schema: Schema): Observable<Schema> {
    return this.AppHttpClient
      .withoutPrefixUrl()
      .post<Schema>(`${this.prefix}`, schema);
  }

  public delete(id: number) {
    return this.AppHttpClient
      .withoutPrefixUrl()
      .delete<Schema>(`${this.prefix}${id}`);
  }


  private closeFullscreen(): SchemaService {
    const doc = (document as any);
    if (doc.cancelFullScreen) {
      doc.cancelFullScreen();
    } else if (doc.mozCancelFullScreen) {
      doc.mozCancelFullScreen();
    } else if (doc.webkitCancelFullScreen) {
      doc.webkitCancelFullScreen();
    }

    return this;
  }

  private makeDiagram(): Diagram {
    return this.NgZone.runOutsideAngular(() => {
      return DiagramFactory(this.schema, this.config, this.diagramElement, this.controlsElement);
    });
  }

  private makeNewSchema(schema: Schema): Schema {
    const { id, type, name, unit_id } = Object.assign({}, schema);
    const instance = <DiagramInstanceConfig>instances.find((instance: DiagramInstanceConfig) => instance.value === schema.type);

    return new instance.schemaClass({ id, type, name, unit_id });
  }
}