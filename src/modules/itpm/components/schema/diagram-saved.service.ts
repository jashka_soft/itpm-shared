import { Injectable } from '@angular/core';

@Injectable()
export class DiagramSavedService {
  private isSaved: boolean = false;

  public isSave(): boolean {
    return this.isSaved;
  }

  public save(): DiagramSavedService {
    this.isSaved = true;

    return this;
  }

  public new(): DiagramSavedService {
    this.isSaved = false;

    return this;
  }
}