import { Lesson } from "./lesson";

export class Course {

  id: number;
  name: string = '';
  description: string = '';
  deleted_at: string | Date | null;
  lessons: Lesson[];

  [key: string]: any;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}