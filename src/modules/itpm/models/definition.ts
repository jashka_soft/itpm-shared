export class Definition {
  [key: string]: any;
  id: number;
  name: string;
  description: string;
  deleted_at: string | Date;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}