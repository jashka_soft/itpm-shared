import { Theory } from "./theory";
import { Test } from "./test/test";
import { Schema } from "../components/schema/schema";
import { User } from '../../auth/model/user';

export class Unit {
  public id: number;
  public name: string = '';
  public description: string = '';
  public lesson_id: number;
  public theory: Theory;
  public test: Test;
  public schema: Schema;
  public deleted_at: string | Date;
  public users_active: User[] = [];

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}