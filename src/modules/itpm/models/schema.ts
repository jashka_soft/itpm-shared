export class Schema {
  id: number;
  type: string;
  name: string = '';
  unit_id: number;
  palette: Array<Object> = [];
  diagram: any | Object = {'class': 'go.TreeModel', nodeDataArray: [],};

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}