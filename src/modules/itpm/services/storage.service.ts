import { Injectable } from "@angular/core";

@Injectable()
export class StorageService {

  public get(key: string, _default?: any): any {
    let data = localStorage.getItem(key);

    if (data) {
      try {
        data = JSON.parse(data);
      } catch (e) {
      }

      return data;
    } else {
      if (_default) {
        return _default;
      }
      return null;
    }
  }

  public set(key: string, value: Object | string | number): StorageService {
    let data = typeof value !== 'string' ? JSON.stringify(value) : value;
    localStorage.setItem(key, data);

    return this;
  }

  public rm(key: string): StorageService {
    localStorage.removeItem(key);

    return this;
  }

}