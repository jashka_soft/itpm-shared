import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";

import { JWTService } from "../../auth/services/jwt.service";
import { AppEnv } from "../itpm.module";

export interface IRequestOptions {
  headers?: HttpHeaders;
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
  body?: any;
}

export function applicationHttpClientFactory(HttpClient: HttpClient, JWTService: JWTService, config: AppEnv) {
  return new AppHttpClient(HttpClient, JWTService, config);
}

@Injectable()
export class AppHttpClient {
  private apiPrefixUrl: string;
  private apiPrefix: string;
  private api: string = '';

  constructor(private HttpClient: HttpClient,
              private JWTService: JWTService,
              private config: AppEnv) {
    this.api = this.config.apiHost;
    this.apiPrefix = this.config.apiPrefix;
    this.apiPrefixUrl = this.config.adminPrefix;
  }

  public getRequestOptionArgs(options?: IRequestOptions): IRequestOptions {

    if (!options) {
      options = {};
    }

    let headers: HttpHeaders = options.headers || new HttpHeaders();

    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', `Bearer ${this.JWTService.getToken()}`);
    options.headers = headers;

    return options;
  }

  protected afterActions(data: any): any {
    this.apiPrefixUrl = this.config.adminPrefix;
    this.apiPrefix = this.config.apiPrefix;

    return data;
  }

  protected getFullPath() {
    return `${this.api + this.apiPrefix + this.apiPrefixUrl}`;
  }

  public withoutPrefixUrl() {
    this.apiPrefixUrl = '';

    return this;
  }

  public get<T>(endPoint: string, options: IRequestOptions = {}): Observable<T> {
    return this.HttpClient
      .get<T>(`${this.getFullPath()}${endPoint}`, this.getRequestOptionArgs(options))
      .map((response: any) => this.afterActions(response.data));
  }

  public post<T>(endPoint: string, body: Object, options: IRequestOptions = {}): Observable<T> {
    return this.HttpClient
      .post<T>(`${this.getFullPath()}${endPoint}`, body, this.getRequestOptionArgs(options))
      .map((response: any) => this.afterActions(response.data));
  }

  public put<T>(endPoint: string, body: Object, options: IRequestOptions = {}): Observable<T> {
    return this.HttpClient
      .put<T>(`${this.getFullPath()}${endPoint}`, body, this.getRequestOptionArgs(options))
      .map((response: any) => this.afterActions(response.data));
  }

  public patch<T>(endPoint: string, body: Object, options: IRequestOptions = {}): Observable<T> {
    return this.HttpClient
      .patch<T>(`${this.getFullPath()}${endPoint}`, body, this.getRequestOptionArgs(options))
      .map((response: any) => this.afterActions(response.data));
  }

  public delete<T>(endPoint: string, options: IRequestOptions = {}): Observable<T> {
    return this.HttpClient
      .delete<T>(`${this.getFullPath()}${endPoint}`, this.getRequestOptionArgs(options))
      .map((response: any) => this.afterActions(response.data));
  }
}