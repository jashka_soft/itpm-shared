import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { PaginationComponent } from "./pagination/pagination.component";
import { PaginationService } from "./pagination/service/pagination.service";
import { AppEnv } from "../itpm/itpm.module";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,

    NgbModule
  ],
  declarations: [
    PaginationComponent
  ],
  providers: [
    PaginationService
  ],
  exports: [
    PaginationComponent
  ]
})
export class PaginationModule {
  public static forRoot(config: AppEnv): ModuleWithProviders {
    return {
      ngModule: PaginationModule,
      providers: [
        {
          provide: AppEnv,
          useValue: config
        }
      ]
    }
  }
}