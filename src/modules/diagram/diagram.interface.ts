import { Schema } from '../itpm/models/schema';

export interface DiagramInterface {
  initDiagram(schema: Schema): any;

  onUpdateModel (): any;

  export(type: TypeExport, schema: Schema): void;

  import(data: any): any;
}

export enum TypeExport {
  PRINT,
  IMAGE,
  PDF,
  FILE
}