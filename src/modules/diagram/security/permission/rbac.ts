import { Permissions } from './permissions';

export class RBAC {

  private permission: Permissions;

  public setupPermission(permission: Permissions) {
    this.permission = permission;

    return this;
  }

  public onRead(fn: Function, another?: Function): any {
    if (this.permission & Permissions.ONLY_READ) {
      return fn();
    }
    if (another && this.permission ^ Permissions.ONLY_READ) {
      return another();
    }
  }

  public onWrite(fn: Function, another?: Function): any {
    if (this.permission & Permissions.USER_ACCESS) {
      return fn();
    }
    if (another && this.permission ^ Permissions.USER_ACCESS) {
      return another();
    }
  }

  public onFull(fn: Function, another?: Function): any {
    if (this.permission & Permissions.FULL_ACCESS) {
      return fn();
    }
    if (another && this.permission ^ Permissions.FULL_ACCESS) {
      return another();
    }
  }

  public onExamination(fn: Function, another?: Function): any {
    if (this.permission & Permissions.EXAMINATION) {
      return fn();
    }
    if (another && this.permission ^ Permissions.EXAMINATION) {
      return another();
    }
  }

  public onPermissions(permissions: Array<Permissions>, fn: Function): any {
    let multi = 0;
    permissions.forEach((perm: Permissions) => {
      multi = multi | perm;
    });

    return this.permission & multi ? fn() : false;
  }

}