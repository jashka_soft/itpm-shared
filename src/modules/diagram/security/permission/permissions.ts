export enum Permissions {
  ONLY_READ = 1 << 0,
  EXAMINATION = 1 << 1,
  USER_ACCESS = 1 << 2,
  FULL_ACCESS = 1 << 3
}