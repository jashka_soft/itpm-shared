import { ElementRef } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

import { DiagramInterface, TypeExport } from '../diagram.interface';
import { DiagramConfig } from '../instances';
import { RBAC } from '../security/permission/rbac';
import { Schema } from '../../itpm/components/schema/schema';

export abstract class Diagram implements DiagramInterface {

  protected div: any = null;
  protected model: any = {};
  protected rbac: RBAC;

  private sourceModel: Subject<any> = new Subject<any>();
  private sourceModel$: Observable<any> = this.sourceModel.asObservable();
  private onResizeSub: Subscription;

  constructor(protected diagramContainer: ElementRef, protected config: DiagramConfig) {
    this.div = diagramContainer.nativeElement;

    this.div.style.width = '100%';
    this.div.style.height = '100%';

    this.initDiagramFullHeight();
    this.initAccess();
  }

  public abstract initDiagram(schema: Schema): any;

  public onUpdateModel<T>(): Observable<T> {
    return this.sourceModel$;
  }

  public destroy() {
    this.onResizeSub.unsubscribe();
    while (this.div && this.div.firstChild) {
      this.div.removeChild(this.div.firstChild);
    }
    this.div = null;
    this.sourceModel.complete();
  }

  public getType(): string {
    return this.config.schema.type;
  }

  public abstract getDiagram(): any;

  public import() {
  }

  public export(type: TypeExport, schema: Schema): void {

    switch (type) {
      /*case TypeExport.FILE:
        this.downloadFile(schema);
        break;
*/
      case TypeExport.IMAGE:
        this.downloadImage(schema);
        break;

      /*case TypeExport.PDF:
        this.downloadPDF(schema);
        break;

      case TypeExport.PRINT:
        this.print();
        break;*/
    }
  }

  protected getSourceModel(): any {
    return {};
  }

  protected updateModel(model: any): Diagram {
    this.sourceModel.next(model);

    return this;
  }

  protected addWatchModel() {
  }

  protected abstract addExampleData(): void;

  // protected abstract downloadFile(schema: Schema): any;

  protected abstract downloadImage(schema: Schema): void;

  // protected abstract downloadPDF(schema: Schema): void;

  // protected abstract print(): void;

  private initDiagramFullHeight() {
    const element = this.config.controlsRef.nativeElement;
    this.diagramFullHeight(element);
    this.onResizeSub = Observable
      .fromEvent(window, 'resize')
      .subscribe(() => {
        this.diagramFullHeight(element);
      });
  }

  private diagramFullHeight(element: HTMLElement) {
    if (window.innerWidth > 767) {
      const domRect = element.getBoundingClientRect();
      const spaceBelow = window.innerHeight - domRect.bottom;
      this.diagramContainer.nativeElement.style.height = `${spaceBelow}px`;
    } else {
      this.diagramContainer.nativeElement.style.height = `600px`;
    }
  }

  private initAccess() {
    this.rbac = new RBAC();
    this.rbac.setupPermission(this.config.permissions);
  }
}