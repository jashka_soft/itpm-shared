import { Node } from 'gojs';

export class PertNode extends Node {
  public length: number = 0;
  public earlyStart: number = 0;
  public lateFinish: number = 0;
  public critical: boolean = false;
}