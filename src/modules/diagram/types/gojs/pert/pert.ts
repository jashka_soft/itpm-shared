import { ElementRef } from '@angular/core';

import * as go from 'gojs';

import { GoJS } from '../gojs';
import { DiagramConfig } from '../../../instances';
import { Schema } from '../../../../itpm/components/schema/schema';
import { Permissions } from '../../../security/permission/permissions';
import { PertNode } from './pert-node';

export class PERT extends GoJS {

  protected exampleNode: any = {
    text: 'змініть текст',
    length: 0,
    earlyStart: 0,
    lateFinish: 0,
    critical: false
  };

  protected model: any = {
    nodeDataArray: [],
    linkDataArray: []
  };
  private blue: string = '#0288D1';
  private pink: string = '#B71C1C';
  private pinkfill: string = '#F8BBD0';
  private bluefill: string = '#B3E5FC';
  private pertPermissions: Array<Permissions> = [
    Permissions.EXAMINATION,
    Permissions.FULL_ACCESS
  ];

  constructor(diagramContainer: ElementRef, config: DiagramConfig) {
    super(diagramContainer, config);

    this.div.classList.add('diagram');

    let canAction = false;

    this.rbac.onPermissions(this.pertPermissions, () => {
      canAction = true;
    });

    let diagramConfig = {
      initialAutoScale: go.Diagram.Uniform,
      initialContentAlignment: go.Spot.Center,
      layout: this.maker(go.LayeredDigraphLayout),
    };
    let nodeTemplate = [
      go.Node, 'Auto',
      this.maker(go.Shape, 'Rectangle',  // the border
        {
          name: 'SHAPE',
          fill: 'white', strokeWidth: 2,
          fromLinkable: canAction, toLinkable: canAction
        },
        new go.Binding('fill', 'critical', (b) => {
          return (b ? this.pinkfill : this.bluefill );
        }),
        new go.Binding('stroke', 'critical', (b) => {
          return (b ? this.pink : this.blue);
        })),
      this.maker(go.Panel, 'Table',
        {padding: 0.5},
        this.maker(go.RowColumnDefinition, {column: 1, separatorStroke: 'black'}),
        this.maker(go.RowColumnDefinition, {column: 2, separatorStroke: 'black'}),
        this.maker(go.RowColumnDefinition, {
          row: 1,
          separatorStroke: 'black',
          background: 'white',
          coversSeparators: true
        }),
        this.maker(go.RowColumnDefinition, {row: 2, separatorStroke: 'black'}),
        this.maker(go.TextBlock, // earlyStart
          this.makeTwoWay(new go.Binding('text', 'earlyStart')),
          this.configBlock({editable: canAction}),
        ),
        this.maker(go.TextBlock,
          this.makeTwoWay(new go.Binding('text', 'length')),
          this.configBlock({column: 1, editable: canAction}),
        ),
        this.maker(go.TextBlock,  // earlyFinish
          new go.Binding('text', '', (d) => (d.earlyStart + d.length).toFixed(2)),
          {row: 0, column: 2, margin: 5, textAlign: 'center'}),

        this.maker(go.TextBlock,
          this.makeTwoWay(new go.Binding('text', 'text')),
          this.configBlock({row: 1, editable: canAction}, {columnSpan: 3, font: 'bold 14px sans-serif'}),
        ),

        this.maker(go.TextBlock,  // lateStart
          new go.Binding('text', '', (d) => (d.lateFinish - d.length).toFixed(2)),
          {row: 2, column: 0, margin: 5, textAlign: 'center'}),
        this.maker(go.TextBlock,  // slack
          new go.Binding('text', '', (d) => (d.lateFinish - (d.earlyStart + d.length)).toFixed(2)),
          {row: 2, column: 1, margin: 5, textAlign: 'center'}),
        this.maker(go.TextBlock, // lateFinish
          this.makeTwoWay(new go.Binding('text', 'lateFinish')),
          this.configBlock({row: 2, column: 2, editable: canAction}),
        )
      )  // end Table Panel

    ];

    this.rbac.onPermissions(this.pertPermissions, () => {
      nodeTemplate.push(
        {doubleClick: this.doubleClickInsertNode.bind(this)},
        this.makeRelinkableNodes(),
      );
      const insertConfig: any = this.getInsertConfig(this.exampleNode);
      diagramConfig = {...diagramConfig, ...insertConfig};
    });

    this.rbac.onRead(() => {
      diagramConfig = {...diagramConfig, ...this.onlyReadPermissions};
    });

    this.diagram =
      this.maker(
        go.Diagram,
        this.div,
        diagramConfig
      );

    // define the Node template
    this.diagram.nodeTemplate = this.maker.apply(this, nodeTemplate);  // end Node;  // end Node

    this.diagram.linkTemplate =
      this.maker(go.Link,
        {
          toShortLength: 6, toEndSegmentLength: 20,
          corner: 5, relinkableFrom: canAction, relinkableTo: canAction
        },
        this.maker(go.Shape,
          {strokeWidth: 4},
          new go.Binding('stroke', '', this.linkColorConverter)),
        this.maker(go.Shape,  // arrowhead
          {toArrow: 'Triangle', stroke: null, scale: 1.5},
          new go.Binding('fill', '', this.linkColorConverter))
      );

    this.rbac.onPermissions(this.pertPermissions, () => {
      this.diagram.nodeTemplate.contextMenu = this.baseContextRemoveNodes();
      this.addIndicatorDiagramModified();
      this.selectionDeleting();
      this.addWatchModel();
    });

    this.findCriticalPath();
    this.addNodeInfo();
  }

  public initDiagram(schema: Schema) {
    super.initDiagram(schema);

    this.runModelDetectionChange();
  }


  protected addExampleData() {
    this.model.nodeDataArray = [
      {key: 1, text: 'Start', length: 0, earlyStart: 0, lateFinish: 0, critical: false},
      {key: 2, text: 'a', length: 4, earlyStart: 0, lateFinish: 4, critical: false},
      /*{key: 3, text: 'b', length: 5.33, earlyStart: 0, lateFinish: 9.17, critical: false},
      {key: 4, text: 'c', length: 5.17, earlyStart: 4, lateFinish: 9.17, critical: false},
      {key: 5, text: 'd', length: 6.33, earlyStart: 4, lateFinish: 15.01, critical: false},
      {key: 6, text: 'e', length: 5.17, earlyStart: 9.17, lateFinish: 14.34, critical: false},
      {key: 7, text: 'f', length: 4.5, earlyStart: 10.33, lateFinish: 19.51, critical: false},
      {key: 8, text: 'g', length: 5.17, earlyStart: 14.34, lateFinish: 19.51, critical: false},
      {key: 9, text: 'Finish', length: 0, earlyStart: 19.51, lateFinish: 19.51, critical: false}*/
    ];

    this.model.linkDataArray = [
      {from: 1, to: 2},
      /*{from: 1, to: 3},
      {from: 2, to: 4},
      {from: 2, to: 5},
      {from: 3, to: 6},
      {from: 4, to: 6},
      {from: 5, to: 7},
      {from: 6, to: 8},
      {from: 7, to: 9},
      {from: 8, to: 9}*/
    ];
  }

  protected makeRelinkableNodes() {
    const dragAndDrop = super.makeRelinkableNodes();

    dragAndDrop.mouseDrop = (e: go.InputEvent, node: go.Node) => {
      let diagram = node.diagram;
      let selectionNode: go.Node = <go.Node>diagram.selection.first();  // assume just one Node in selection
      if (this.mayWorkFor(selectionNode, node)) {
        // find any existing link into the selected node
        let link = selectionNode.findTreeParentLink();
        if (link !== null) {  // reconnect any existing link
          link.fromNode = node;
          this.findCriticalPath();
        } else {  // else create a new link
          diagram.toolManager.linkingTool.insertLink(node, node.port, selectionNode, selectionNode.port);
        }
      }
    }
    ;

    return dragAndDrop;
  }


  private linkColorConverter(linkdata: any, elt: go.GraphObject) {
    let link: go.Link = <go.Link>elt.part;
    if (!link) return this.blue;
    let node = link.fromNode;
    if (!node || !node.data || !node.data.critical) return this.blue;
    let node1 = link.toNode;
    if (!node1 || !node1.data || !node1.data.critical) return this.blue;
    return this.pink;  // when both Link.fromNode.data.critical and Link.toNode.data.critical
  }

  private addNodeInfo(): void {
    this.diagram.add(
      this.maker(go.Node, 'Auto',
        this.maker(go.Shape, 'Rectangle',  // the border
          {fill: this.bluefill}),
        this.maker(go.Panel, 'Table',
          this.maker(go.RowColumnDefinition, {column: 1, separatorStroke: 'black'}),
          this.maker(go.RowColumnDefinition, {column: 2, separatorStroke: 'black'}),
          this.maker(go.RowColumnDefinition, {
            row: 1,
            separatorStroke: 'black',
            background: this.bluefill,
            coversSeparators: true
          }),
          this.maker(go.RowColumnDefinition, {row: 2, separatorStroke: 'black'}),
          this.maker(go.TextBlock, this.addEditableText('Early Start'),
            {row: 0, column: 0, margin: 5, textAlign: 'center'}),
          this.maker(go.TextBlock, this.addEditableText('Length'),
            {row: 0, column: 1, margin: 5, textAlign: 'center'}),
          this.maker(go.TextBlock, 'Early Finish',
            {row: 0, column: 2, margin: 5, textAlign: 'center'}),
          this.maker(go.TextBlock, this.addEditableText('Activity Name'),
            {
              row: 1, column: 0, columnSpan: 3, margin: 5,
              textAlign: 'center', font: 'bold 14px sans-serif'
            }),
          this.maker(go.TextBlock, 'Late Start',
            {row: 2, column: 0, margin: 5, textAlign: 'center'}),
          this.maker(go.TextBlock, 'Slack',
            {row: 2, column: 1, margin: 5, textAlign: 'center'}),
          this.maker(go.TextBlock, this.addEditableText('Late Finish'),
            {row: 2, column: 2, margin: 5, textAlign: 'center'})
        )  // end Table Panel
      ));
  }

  private addEditableText(text: string): string {
    const result = this.rbac.onPermissions(this.pertPermissions, () => {
      return ' (editable)';
    });

    return `${text}${ result ? result : '' }`;
  }

  private findCriticalPath(): void {
    const rootData: any = this.diagram.model.nodeDataArray.find((node: go.Node | any) => !node.hasOwnProperty('parent'));
    if (rootData) {
      const first = this.diagram.findNodeForKey(rootData.key);
      if (first && first.data) {
        const child = first.findTreeChildrenNodes();
        const paths = this.newPath(child, `${this.getNodeIdentificator(first)}:`, []);
        this.findMaxPathNode(paths);
      }
    }
  }

  private findMaxPathNode(paths: Array<string>): void {
    let pathNodes = [];
    for (let value of paths) {
      value = value.substring(0, value.length - 1);
      const nodeIds = value.split(':');
      const len = nodeIds.length;
      pathNodes.push({
        path: value,
        len: len,
        nodeIds: nodeIds.map(id => parseInt(id))
      });
    }
    if (pathNodes.length > 0) {
      const path = pathNodes.reduce(function (prev, current) {
        return (prev.len > current.len) ? prev : current
      });
      this.highlightMaxPath(path.nodeIds);
    }
  }

  private highlightMaxPath(nodeIds: Array<number>): void {
    this.diagram.nodes.map(node => {
      if (node.data) {
        this.diagram.model.setDataProperty(node.data, 'critical', false);
      }
    });

    nodeIds.forEach(id => {
      const node = this.diagram.findNodeForKey(id);
      this.diagram.model.setDataProperty(node.data, 'critical', true);
    });
  }

  private getNodeIdentificator(node: go.Node): any {
    return node.data.key;
  }

  private children(node: go.Node, nodePath: string, paths: Array<string>): string | any {
    const childs = node.findTreeChildrenNodes();
    if (childs.count) {
      this.newPath(childs, nodePath, paths);
    } else {
      return nodePath;
    }
  }

  private newPath(nodes: go.Iterator<go.Node>, nodePath: string, paths: Array<string>): Array<string> {
    while (nodes.next()) {
      const node = nodes.value;
      const path = this.children(node, `${nodePath ? nodePath : ''}${this.getNodeIdentificator(node)}:`, paths);
      if (typeof path === 'string') {
        paths.push(path);
      }
    }

    return paths;
  }

  private runModelDetectionChange() {
    this.diagram.model.addChangedListener((e: go.ChangedEvent) => {
      const nodesChange = e.propertyName === 'nodeDataArray'
        || e.propertyName === 'linkDataArray'
        || e.propertyName === 'from';
      if (nodesChange) {
        this.findCriticalPath();
      }
    });
  }

  protected makeTwoWay(binding: go.Binding): go.Binding {
    const result = this.rbac.onPermissions(this.pertPermissions, () => binding.makeTwoWay());

    return result ? result : binding;
  }
}