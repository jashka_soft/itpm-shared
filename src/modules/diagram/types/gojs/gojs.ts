import { ElementRef } from '@angular/core';

import * as go from 'gojs';
import * as jspdf from 'jspdf';

import { Diagram } from '../diagram';
import { DiagramConfig } from '../../instances';
import { Schema } from '../../../itpm/components/schema/schema';

/**
 * TODO: add focus to diagram
 */
export abstract class GoJS extends Diagram {

  protected exampleNode: any;
  protected maker: Function;
  protected diagram: go.Diagram;
  protected levelColors: Array<string> = [
    '#157e48',
    '#AC193D',
    '#2672EC',
    '#8C0095',
    '#5133AB',
    '#008A00',
    '#008299',
    '#D24726',
    '#094AB2',
  ];
  protected nodeIdCounter: number = -1;
  protected onlyReadPermissions: any = {
    allowClipboard: false,
    allowCopy: false,
    allowDelete: false,
    allowDragOut: false,
    allowDrop: false,
    allowGroup: false,
    allowInsert: false,
    allowLink: false,
    allowMove: false,
    allowRelink: false,
    allowTextEdit: false,
  };

  constructor(elementRef: ElementRef, config: DiagramConfig) {
    super(elementRef, config);

    this.maker = go.GraphObject.make;
  }

  public destroy() {
    super.destroy();
    if (this.diagram) {
      delete this.diagram.div;
      (this.diagram as any).div = null;
      this.diagram.model.clear();
      this.diagram.clear();
    }
    this.model.nodeDataArray = [];
    this.model.linkDataArray = [];
  }

  public getDiagram() {
    return this.diagram;
  }

  public initDiagram(schema: Schema) {
    let nodeDataArray = [],
        linkDataArray = [];
    if (schema.diagram) {
      nodeDataArray = schema.diagram.nodeDataArray;
      linkDataArray = schema.diagram.linkDataArray;
    }
    this.diagram.model = new go.GraphLinksModel(nodeDataArray, linkDataArray);
  }


  protected getSourceModel(): any {
    return JSON.parse(this.diagram.model.toJson());
  }

  protected print(image?: HTMLImageElement) {
    image = image || this.makeImage();
    const width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    const height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;

    let windowImage = window.open('', 'PRINT', `height=${height},width=${width}`);

    let htmlSource = `<html><head><title></title>
        </head><body onload='window.print()'>
        <img src='${image.src}' />
        </body></html>`;
    windowImage.document.write(htmlSource);

    windowImage.document.close(); // necessary for IE >= 10
    windowImage.focus(); // necessary for IE >= 10*/
  }

  protected downloadFile(schema: Schema) {
    let dataSource = {
      type: schema.type,
      name: schema.name,
      unit_id: schema.unit_id,
      diagram: this.getSourceModel(),
    };

    const source = JSON.stringify(dataSource);
    this.download(source, schema);
  }

  protected downloadImage(schema: Schema) {
    const image = this.makeImage();
    this.download(image.src, schema);
  }

  protected downloadPDF(schema: Schema) {
    const image = this.makeImage();
    const name = `${schema.name ? schema.name : ''} - ${schema.type} - ${new Date().toDateString()}.pdf`;
    let pdf = new jspdf();
    pdf.addImage(image.src, 'JPEG', 1, 1);
    pdf.save(name);
    /*import('jspdf/dist/jspdf.min.js').then(module => {

     });*/
  }

  protected selectionDeleting(removeItem?: Function) {
    // manage boss info manually when a node or link is deleted from the diagram
    this.diagram.addDiagramListener('SelectionDeleting', (e: go.DiagramEvent) => {

      let part = e.subject.first(); // e.subject is the myDiagram.selection collection,
                                    // so we'll get the first since we know we only have one selection
      this.diagram.startTransaction('clear boss');
      if (part instanceof go.Node) {
        if (removeItem) {
          removeItem(part.data);
        }

        let it: go.Iterator<go.Node> = part.findTreeChildrenNodes(); // find all child nodes
        while (it.next()) { // now iterate through them and clear out the boss information
          let child = it.value;
          let bossText: go.TextBlock = <go.TextBlock>child.findObject('boss'); // since the boss TextBlock is named, we can access it by name
          if (bossText === null) return;
          bossText.text = '';
        }
      } else if (part instanceof go.Link) {
        let child: go.Node = part.toNode;
        let bossText: go.TextBlock = <go.TextBlock>child.findObject('boss'); // since the boss TextBlock is named, we can access it by name
        if (bossText === null) return;
        bossText.text = '';
      }
      this.diagram.commitTransaction('clear boss');
    });
  }

  protected commitNodes() {
    go.TreeLayout.prototype.commitNodes.call(this.diagram.layout);  // do the standard behavior
    // then go through all of the vertexes and set their corresponding node's Shape.fill
    // to a brush dependent on the TreeVertex.level value
    this.diagram.layout.network.vertexes.each((v: go.TreeVertex) => {
      if (v.node) {
        let level = v.level % (this.levelColors.length);
        let color = this.levelColors[level];
        let shape: go.Shape = <go.Shape>v.node.findObject('SHAPE');
        if (shape) shape.fill = this.maker(go.Brush, 'Linear', {
          0: color,
          1: go.Brush.lightenBy(color, 0.05),
          start: go.Spot.Left,
          end: go.Spot.Right
        });
      }
    });
  }

  protected baseContextRemoveNodes(removeItem?: Function) {
    return this.maker(go.Adornment, 'Vertical',
      /*this.maker('ContextMenuButton',
        this.maker(go.TextBlock, 'Удалить блок (только без дочерних)'),
        {
          click: (e: go.InputEvent, obj: go.GraphObject) => {
            // reparent the subtree to this node's boss, then remove the node
            let node: go.Node = <go.Node>(obj.part as go.Adornment).adornedPart;
            if (node !== null) {
              this.diagram.startTransaction('reparent remove');
              let chl = node.findTreeChildrenNodes();
              // iterate through the children and set their parent key to our selected node's parent key
              while (chl.next()) {
                let emp: go.Node = chl.value;
                (this.diagram.model as go.TreeModel).setParentKeyForNodeData(emp.data, node.findTreeParentNode().data.key);
              }

              if (removeItem) {
                removeItem(node.data);
              }

              // and now remove the selected node itself
              this.diagram.model.removeNodeData(node.data);

              this.diagram.commitTransaction('reparent remove');
            }
          }
        }
      ),*/
      this.maker('ContextMenuButton',
        this.maker(go.TextBlock, 'Видалити гілку'),
        {
          click: (e: go.InputEvent, obj: go.GraphObject) => {
            // remove the whole subtree, including the node itself
            let node: go.Node = <go.Node>(obj.part as go.Adornment).adornedPart;
            if (node !== null) {
              this.diagram.startTransaction('remove dept');

              const parts: go.Set<go.Part> = node.findTreeParts();

              if (removeItem) {
                parts.map((part: go.Part) => {
                  removeItem(part.data);
                });
              }

              this.diagram.removeParts(parts, false);
              this.diagram.commitTransaction('remove dept');
            }
          }
        }
      )
    );
  }

  protected doubleClickInsertNode(e: go.InputEvent, obj: go.GraphObject): any {
    let clicked: go.Part = obj.part;
    if (clicked !== null) {
      let parentNode: go.Node = clicked.data;
      this.diagram.startTransaction('add node');
      let node = this.getNode(this.getNextKey(), this.exampleNode, parentNode.key);
      this.diagram.model.addNodeData(node);
      this.diagram.commitTransaction('add node');

      (this.diagram.model as go.GraphLinksModel)
        .addLinkData({from: parentNode.key, to: node.key});

      return {parentNode, node};
    }

    return null;
  }

  protected getNextKey(): number {
    let key = this.nodeIdCounter;
    while (this.diagram.model.findNodeDataForKey(key) !== null) {
      key = this.nodeIdCounter--;
    }
    return key;
  }

  protected addIndicatorDiagramModified() {
    // when the document is modified, add a '*' to the title and enable the 'Save' button
    this.diagram.addDiagramListener('Modified', (e: go.DiagramEvent) => {
      let idx = document.title.indexOf('*');
      if (this.diagram.isModified) {
        if (idx < 0) document.title += '*';
      } else {
        if (idx >= 0) document.title = document.title.substr(0, idx);
      }
    });
  }

  protected makeRelinkableNodes() {
    return { // handle dragging a Node onto a Node to (maybe) change the reporting relationship
      mouseDragEnter: (e: go.InputEvent, node: go.Node, prev: go.GraphObject) => {
        let diagram: go.Diagram = node.diagram;
        let selnode: go.Part = diagram.selection.first();
        if (!this.mayWorkFor(selnode, node)) return;
        let shape: any | go.Shape = <go.Shape>node.findObject('SHAPE');
        if (shape) {
          shape._prevFill = shape.fill;  // remember the original brush
          shape.fill = 'darkred';
        }
      },
      mouseDragLeave: (e: go.InputEvent, node: go.Node, next: go.InputEvent) => {
        let shape: go.Shape | any = node.findObject('SHAPE');
        if (shape && shape._prevFill) {
          shape.fill = shape._prevFill;  // restore the original brush
        }
      },
      mouseDrop: (e: go.InputEvent, node: go.Node) => {
        let diagram = node.diagram;
        let selectionNode: go.Node = <go.Node>diagram.selection.first();  // assume just one Node in selection
        if (this.mayWorkFor(selectionNode, node)) {
          // find any existing link into the selected node
          let link = selectionNode.findTreeParentLink();
          if (link !== null) {  // reconnect any existing link
            link.fromNode = node;
          } else {  // else create a new link
            diagram.toolManager.linkingTool.insertLink(node, node.port, selectionNode, selectionNode.port);
          }
        }
      }
    }
  }

  protected getNode(nextKey: number, nodeData: any, parent?: go.Key) {
    let node: any = {
      key: nextKey,
    };

    if (parent) {
      node['parent'] = parent;
    }

    node = Object.assign(node, nodeData);

    return node;
  }

  protected addWatchModel() {
    this.diagram.addModelChangedListener((e: go.ChangedEvent) => {
      if (e.isTransactionFinished) {
        this.config.schema.diagram = this.getSourceModel();
        this.updateModel(this.config.schema);
      }
    });
  }

  protected highlight(node: go.Node | null, diagram: go.Diagram) {  // may be null
    let oldSkips = diagram.skipsUndoManager;
    diagram.skipsUndoManager = true;
    diagram.startTransaction('highlight');
    if (node !== null) {
      diagram.highlight(node);
    } else {
      diagram.clearHighlighteds();
    }
    diagram.commitTransaction('highlight');
    diagram.skipsUndoManager = oldSkips;
  }

  protected configBlock(config: any = {}, extend?: any): any {

    config = {
      row: config.row || 0,
      column: config.column || 0,
      margin: config.margin || 5,
      textAlign: config.textAlign || 'center',
      editable: config.editable || false,
      isMultiline: config.textAlign || true
    };

    if (extend) {
      config = {...config, ...extend};
    }

    return config;
  }

  protected getInsertConfig(exampleNode: any): any {
    const self = this;
    return {
      'undoManager.isEnabled': true,
      'clickCreatingTool.archetypeNodeData': {}, // allow double-click in background to create a new node
      'clickCreatingTool.insertPart': function (loc: go.Point) {  // customize the data for the new node
        this.archetypeNodeData = self.getNode(self.getNextKey(), exampleNode);
        if (self.canAddRootNode()) {
          go.ClickCreatingTool.prototype.insertPart.call(this, loc);
        }
      }
    };
  }

  protected mayWorkFor(node1: go.Part, node2: go.Node): boolean {
    if (!(node1 instanceof go.Node)) return false;  // must be a Node
    if (node1 === node2) return false;  // cannot work for yourself
    if (node2.isInTreeOf(node1)) return false;  // cannot work for someone who works for you
    return true;
  }


  private canAddRootNode(): boolean {
    let countRootNodes = 0;
    this.diagram.nodes.each((node: go.Node | any) => {
      if (node.data && !node.parent) {
        countRootNodes++;
      }
    });
    return countRootNodes < 1;
  }

  private makeImage(scale: number = 0.8): HTMLImageElement {
    return this.diagram.makeImage({
      scale: scale
    });
  }

  private download(source: string, schema: Schema) {
    const link = document.createElement('a');
    const name = `${schema.name ? schema.name : ''} - ${schema.type} - ${new Date().toDateString()}`;
    link.setAttribute('href', source);
    link.setAttribute('download', name);
    link.click();
  }
}