import { ElementRef } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs';

import * as go from 'gojs';

import { DiagramConfig } from '../../../instances';
import { GoJS } from '../gojs';
import { Permissions } from '../../../security/permission/permissions';
import { WBSSchema } from '../../../../itpm/components/schema/schema';
import { deepCopy } from '../../../../itpm/components/schema/schema.component';

export const PALETTE_NAME: string = 'palette';

export class WBS extends GoJS {

  protected exampleNode: any = {
    name: '(назва)',
    title: '',
    //hash: this.hash()
  };

  private items: Array<go.Node> = [];
  private itemsSource: Subject<Array<go.Node>> = new Subject<Array<go.Node>>();
  private itemsObservable: Observable<Array<go.Node>> = this.itemsSource.asObservable();
  private draggableElement: HTMLElement | any;
  private subItems: Subscription;
  private palette: Node;
  private refWBS: Node;

  constructor(diagramContainer: ElementRef, config: DiagramConfig) {
    super(diagramContainer, config);

    let diagramConfig: any = {
      initialContentAlignment: go.Spot.Center,
      maxSelectionCount: 1, // users can select only one part at a time
      validCycle: go.Diagram.CycleDestinationTree, // make sure users can only create trees

      layout: this.maker(go.TreeLayout,
        {
          treeStyle: go.TreeLayout.StyleLastParents,
          arrangement: go.TreeLayout.ArrangementHorizontal,
          // properties for most of the tree:
          angle: 90,
          layerSpacing: 35,
          alignment: go.TreeLayout.AlignmentCenterChildren,
          // properties for the 'last parents':
          alternateAngle: 90,
          alternateLayerSpacing: 35,
          alternateNodeSpacing: 20
        }),
    };

    this.rbac.onFull(() => {
      const insertConfig: any = this.getInsertConfig(this.exampleNode);
      diagramConfig = {...diagramConfig, ...insertConfig};
    });

    this.rbac.onPermissions([Permissions.EXAMINATION, Permissions.ONLY_READ], () => {
      diagramConfig = {...diagramConfig, ...this.onlyReadPermissions};
    });


    const wbs = this.getDiagramElement();
    this.refWBS = this.div.appendChild(wbs);

    if (this.existPalette()) {
      const {root, palette} = this.makePalette();
      this.div.appendChild(root);
      this.palette = palette;
    }

    // disable relink nodes to node, drag and drop only
    diagramConfig.allowLink = false;

    this.diagram =
      this.maker(
        go.Diagram,
        this.refWBS, // must be the ID or reference to div
        diagramConfig);

    // define the Node template
    let nodeTemplate = this.getNodeTemplate();

    this.rbac.onFull(() => {
      nodeTemplate.push(
        {doubleClick: this.doubleClickInsertNode.bind(this)},
        this.makeRelinkableNodes(),
      );
    });

    this.rbac.onExamination(() => {
      nodeTemplate.push(
        { // handle dragging a Node onto a Node to (maybe) change the reporting relationship
          mouseDragEnter: (e: go.InputEvent, node: go.Node, prev: go.GraphObject) => {
            let shape: go.Shape | any = node.findObject('SHAPE');
            if (shape) {
              shape._prevFill = shape.fill;  // remember the original brush
              shape.fill = 'darkred';
            }
          },
          mouseDragLeave: (e: go.InputEvent, node: go.Node, next: go.InputEvent) => {
            let shape: go.Shape | any = node.findObject('SHAPE');
            if (shape && shape._prevFill) {
              shape.fill = shape._prevFill;  // restore the original brush
            }
          },
        }
      );
    });

    this.diagram.nodeTemplate = this.maker.apply(this, nodeTemplate);  // end Node

    this.rbac.onFull(() => {
      // the context menu allows users to make a position vacant,
      // remove a role and reassign the subtree, or remove a department
      this.diagram.nodeTemplate.contextMenu = this.baseContextRemoveNodes(this.removeItem.bind(this));
    });

    // define the Link template
    this.diagram.linkTemplate =
      this.maker(go.Link, go.Link.Orthogonal,
        {corner: 5},
        this.maker(go.Shape, {strokeWidth: 4, stroke: '#00a4a4'}));  // the link shape

    this.addIndicatorDiagramModified();

    this.rbac.onFull(() => {
      this.selectionDeleting(this.removeItem.bind(this));
    });

    const layout = (this.diagram.layout as go.TreeLayout);
    // override TreeLayout.commitNodes to also modify the background brush based on the tree depth level
    layout.commitNodes = this.rbac.onExamination(
      () => this.examinationCommitNodes.bind(this),
      () => this.commitNodes.bind(this)
    );

    (window as any)['PIXELRATIO'] = this.diagram.computePixelRatio();

    this.addWatchModel();
  }

  public initDiagram(schema: WBSSchema) {
    super.initDiagram(schema);

    if (this.existPalette()) {
      this.subItems = this.onItemsUpdated().subscribe(() => this.renderPalette());
      this.initItems(<Array<go.Node>>schema.diagram.palette || []);
    }

    this.makeCounters(<Array<go.Node>>this.diagram.model.nodeDataArray);
    this.runModelDetectionChange();
    this.setupDragAndDrop();
  }

  public destroy() {
    super.destroy();
    this.items = [];
    this.itemsSource.complete();
    if (this.subItems) {
      this.subItems.unsubscribe();
    }
  }


  protected makeRelinkableNodes() {
    const dragAndDrop = super.makeRelinkableNodes();

    dragAndDrop.mouseDrop = (e: go.InputEvent, node: go.Node) => {
      let diagram = node.diagram;
      let selectionNode: go.Node = <go.Node>diagram.selection.first();  // assume just one Node in selection
      if (this.mayWorkFor(selectionNode, node)) {
        // find any existing link into the selected node
        let link = selectionNode.findTreeParentLink();
        if (link !== null) {  // reconnect any existing link
          this.diagram.model.setDataProperty(selectionNode.data, 'parent', node.data.key);
          link.fromNode = node;
        } else {  // else create a new link
          diagram.toolManager.linkingTool.insertLink(node, node.port, selectionNode, selectionNode.port);
        }
      }
    }
    ;

    return dragAndDrop;
  }

  protected getSourceModel(): any {
    let model = super.getSourceModel();
    model[PALETTE_NAME] = this.items;

    return model;
  }

  protected onItemsUpdated() {
    return this.itemsObservable;
  }

  protected itemContains(node: go.Node): go.Node | undefined {
    return this.items.find((_node: go.Node) => _node.key === node.key);
  }

  protected initItems(items: Array<go.Node>) {
    this.items = items;
    this.itemsSource.next(this.items);
  }

  protected addItem(node: go.Node) {
    this.items.push(node);
    this.itemsSource.next(this.items);
  }

  protected removeItem(node: go.Node) {
    this.items = this.items.filter(_node => node !== _node);
    this.itemsSource.next(this.items);
  }

  protected addExampleData() {
    this.model.nodeDataArray.push({'key': 1, 'name': 'some name', 'title': 'title',});
  }

  private runModelDetectionChange() {
    this.diagram.model.addChangedListener((e: go.ChangedEvent) => {
      // const nodesChange = e.propertyName === 'CommittedTransaction';
      const nodesChange = e.propertyName === 'nodeDataArray'
        || e.propertyName === 'linkDataArray'
        || e.propertyName === 'from';
      if (nodesChange) {
        this.makeCounters(<Array<go.Node>>this.diagram.model.nodeDataArray);
      }
    });
  }


  private existPalette(): boolean {
    return this.config.data['role'] === 'admin'
      || this.config.permissions === Permissions.EXAMINATION;
  }

  // This function provides a common style for most of the TextBlocks.
  // Some of these values may be overridden in a particular TextBlock.
  private textStyle() {
    return {font: '9pt  Segoe UI,sans-serif', stroke: 'white'};
  }

  private getNodeTemplate(): Array<any> {

    let nodeOuterShapeConfig: Array<any> = [
      go.Shape,
      'Rectangle',
      {
        name: 'SHAPE', fill: 'white', stroke: null,
        // set the port properties:
        portId: '', fromLinkable: true, toLinkable: true, cursor: 'pointer'
      },
    ];

    this.rbac.onExamination(() => {
      nodeOuterShapeConfig.push(
        new go.Binding('fill', 'isHighlighted', (isHighlighted: any, shape: go.Shape) => {
          const choice = shape.part.data.choice;
          if (choice && choice === true) {
            if (isHighlighted) {
              return 'red';
            } else {
              return this.maker(go.Brush, 'Linear', {
                0: '#2672EC',
                1: go.Brush.lightenBy('#2672EC', 0.15),
                start: go.Spot.Top,
                end: go.Spot.Bottom
              });
            }
          } else {
            return shape.fill;
          }
        }).ofObject()
      );
    });

    let tableConfig = [
      go.Panel,
      'Table',
      {
        maxSize: new go.Size(150, 999),
        margin: new go.Margin(6, 10, 0, 3),
        defaultAlignment: go.Spot.Left
      },
      this.maker(go.RowColumnDefinition, {column: 3, width: 4}),
      this.maker(
        go.TextBlock,
        this.textStyle(),  // the name
        this.configBlock({
            row: 1,
            // columnSpan: 5,
            editable: true,
          },
          {
            minSize: new go.Size(10, 16),
            font: '12pt Segoe UI,sans-serif'
          }),
          this.getNameFieldText()
      ),

      this.maker(
        go.TextBlock,
        this.textStyle(),  // the counter
        this.configBlock({
            columnSpan: 5,
            row: 2,
          },
          {
            editable: false,
            minSize: new go.Size(10, 16),
            font: '12pt Segoe UI,sans-serif'
          }),
        new go.Binding('text', 'counter').makeTwoWay()
      )
    ];

    this.rbac.onFull(() => {
      if (this.config.data['role'] === 'admin') {
        tableConfig.push(
          this.maker('CheckBox', 'choice',
            {
              name: 'CHOICE_HIDDEN',
              row: 3,
              column: 0,
              'Button.width': 20,
              'Button.height': 20,
              'ButtonBorder.figure': 'Circle',
              'ButtonBorder.stroke': 'blue',
              'ButtonIcon.figure': 'Circle',
              'ButtonIcon.fill': 'blue',
              'ButtonIcon.strokeWidth': 0,
              'ButtonIcon.desiredSize': new go.Size(10, 10)
            },
            this.maker(go.TextBlock, {
              text: 'приховати',
              name: 'CHECKBOXDA'
            }),
            {
              '_doClick': (e: any, obj: go.Shape) => {
                const node = obj.part.data;
                const choice = obj.part.data.choice;

                if (choice && !this.itemContains(node)) {
                  this.addItem(node);
                } else {
                  let _node = this.itemContains(node);
                  if (_node) {
                    this.removeItem(_node);
                  }
                }
              }
            }
          )
        );
      }
    });

    return [
      go.Node,
      'Auto',
      // for sorting, have the Node.text be the data.name
      new go.Binding('text', 'name'),
      // bind the Part.layerName to control the Node's layer depending on whether it isSelected
      new go.Binding('layerName', 'isSelected', function (sel) {
        return sel ? 'Foreground' : '';
      }).ofObject(),
      // define the node's outer shape
      this.maker.apply(this, nodeOuterShapeConfig),
      this.maker(go.Panel, 'Horizontal',
        // define the panel where the text will appear
        this.maker.apply(this, tableConfig)  // end Table Panel
      ) // end Horizontal Panel

    ]
  }

  private getNameFieldText() {
    if (this.config.permissions === Permissions.EXAMINATION) {
      return new go.Binding('text', 'name', function (value, obj: any) {
        const partData = obj['part']['data'];
        const choice = partData['choice'];
        if (partData['dropped']) {
          return partData['dropped_data']['name'];
        } else if (choice && choice === true && !partData['dropped']) {
          return 'Перемістіть сюда';
        } else {
          return value;
        }
      });
    }

    if (this.config.permissions === Permissions.ONLY_READ) {
      return new go.Binding('text', 'name', function (value, obj: any) {
        const partData = obj['part']['data'];
        const choice = partData['choice'];
        if (partData['result'] && partData['result']['result'] === true) {
          return `${partData['dropped_data']['name']} - Правильно`;
        } else if (choice && choice === true && !partData['dropped']) {
          return `${partData['dropped_data']['name']} - Неправильно\nПравильный ответ - ${partData['name']}`;
        } else {
          return value;
        }
      });
    }

    return new go.Binding('text', 'name').makeTwoWay();
  }

  private makeCounters(nodes: Array<go.Node>) {
    nodes = deepCopy(nodes);
    const len = nodes.length;

    for (let i = 0; i < len; ++i) {
      this.diagram.model.setDataProperty(nodes[i], 'counter', '');
    }

    for (let i = 0; i < len; ++i) {
      const key: go.Key = <go.Key>nodes[i].key;
      const node = this.diagram.findNodeForKey(key);
      if (node.data.parent) {
        const level = node.findTreeLevel();
        const parent = this.diagram.findNodeForKey(node.data.parent);
        const byLevel = this.getNodesInLevel(level, nodes);
        const byParent = this.filterByParent(node, byLevel);
        let index: number = <number>this.findNodeIndex(node, byParent);
        ++index;
        let wbsCount = index.toString();
        if (parent && level > 1) {
          wbsCount = `${parent.data.counter}.${index.toString()}`;
        }
        this.diagram.model.setDataProperty(node.data, 'counter', wbsCount);
      }
    }
  }

  private findNodeIndex(node: go.Node, nodes: Array<go.Node>): number | null {
    const len = nodes.length;
    for (let i = 0; i < len; ++i) {
      if (node.data.key === nodes[i].key) {
        return i;
      }
    }

    return null;
  }

  private filterByParent(current: go.Node, nodes: Array<go.Node>): Array<go.Node> {
    return nodes.filter((node: go.Node | any) => node.parent === current.data.parent);
  }

  private getNodesInLevel(level: number, nodes: Array<go.Node>) {
    let nodeInLevel = [];
    const len = nodes.length;

    for (let i = 0; i < len; ++i) {
      const node = this.diagram.findNodeForKey((nodes[i].key as go.Key));
      const nodeLevel = node.findTreeLevel();
      if (nodeLevel === level) {
        nodeInLevel.push(nodes[i]);
      }
    }

    return nodeInLevel;
  }


  private examinationCommitNodes() {
    go.TreeLayout.prototype.commitNodes.call(this.diagram.layout);  // do the standard behavior
    // then go through all of the vertexes and set their corresponding node's Shape.fill
    // to a brush dependent on the TreeVertex.level value
    this.diagram.layout.network.vertexes.each((v: go.TreeVertex) => {
      if (v.node) {
        let level = v.level % (this.levelColors.length);
        let color = this.levelColors[level];
        let shape: go.Shape = <go.Shape>v.node.findObject('SHAPE');
        if (shape) {
          if (shape.part.data.choice && shape.part.data.choice === true) {
            shape.fill = this.maker(go.Brush, 'Linear', {
              0: '#2672EC',
              1: go.Brush.lightenBy('#2672EC', 0.15),
              start: go.Spot.Top,
              end: go.Spot.Bottom
            });
          } else {
            shape.fill = this.maker(go.Brush, 'Linear', {
              0: color,
              1: go.Brush.lightenBy(color, 0.05),
              start: go.Spot.Left,
              end: go.Spot.Right
            });
          }
        }
      }
    });
  }

  private getCurrentNode(event: MouseEvent) {
    let can: Element = <Element>event.target;
    let pixelratio = (window as any)['PIXELRATIO'];

    // if the target is not the canvas, we may have trouble, so just quit:
    if (!(can instanceof HTMLCanvasElement)) return;

    let bbox = can.getBoundingClientRect();
    let bbw = bbox.width;
    if (bbw === 0) bbw = 0.001;
    let bbh = bbox.height;
    if (bbh === 0) bbh = 0.001;
    let mx = event.clientX - bbox.left * ((can.width / pixelratio) / bbw);
    let my = event.clientY - bbox.top * ((can.height / pixelratio) / bbh);
    let point = this.diagram.transformViewToDoc(new go.Point(mx, my));
    return this.diagram.findPartAt(point, true);
  }

  private makePalette() {
    const root = document.createElement('div');
    root.classList.add('col-md-2');

    const header = document.createElement('h6');
    header.textContent = 'Приховані елементи';

    const palette = document.createElement('div');
    palette.style.height = '100%';
    palette.classList.add('palette');
    palette.id = 'palette';

    root.appendChild(header);
    root.appendChild(palette);

    return {root, palette};
  }

  private renderPalette() {
    const items = this.getPaletteItems();
    while (this.palette.firstChild) {
      this.palette.removeChild(this.palette.firstChild);
    }
    items.forEach((item) => {
      this.palette.appendChild(item);
    });
  }

  private getPaletteItems() {
    return this.items.map((item: go.Node) => {
      return this.makePaletteItem(item);
    });
  }

  private makePaletteItem(item: go.Node) {
    const p = document.createElement('p');
    p.setAttribute('data-h', <string>item.key);
    p.classList.add('palette-item');
    p.setAttribute('draggable', 'true');
    p.textContent = item.name;

    return p;
  }

  private getDiagramElement() {
    const wbs = document.createElement('div');
    wbs.classList.add(this.existPalette() ? 'col-md-10' : 'col-md-12');

    return wbs;
  }

  private setupDragAndDrop() {
    this.rbac.onExamination(() => {

      this.refWBS.addEventListener('dragover', (event: MouseEvent) => {
        let currentNode = this.getCurrentNode(event);
        if (currentNode instanceof go.Node) {
          this.highlight(currentNode, this.diagram);
        } else {
          this.highlight(null, this.diagram);
        }

        if ((event.target as Element).className === 'dropzone') {
          // Disallow a drop by returning before a call to preventDefault:
          return;
        }

        // Allow a drop on everything else
        event.preventDefault();

      }, false);

      document.addEventListener('dragstart', (event) => {
        this.draggableElement = <HTMLElement>event.target;
      });

      this.refWBS.addEventListener('drop', (event: MouseEvent) => {

        event.preventDefault();

        let curnode = this.getCurrentNode(event);

        if (curnode instanceof go.Node) {
          const existChoiceNotDropped = curnode.data.choice
            && curnode.data.choice === true
            && !curnode.data.dropped;

          if (existChoiceNotDropped) {
            this.diagram.startTransaction('update_state');

            curnode.data.dropped = true;
            curnode.data.dropped_data = this.items.find((item: go.Node) => item.key === +this.draggableElement.getAttribute('data-h'));

            let backup = Object.assign({}, curnode.data);

            curnode.data = {};
            curnode.data = backup;

            const items = this.items.filter((item: go.Node) => item.key !== +this.draggableElement.getAttribute('data-h'));

            this.initItems(items);

            this.diagram.commitTransaction('update_state');
          }
        } else {
          return;
        }

      }, false);
    });
  }

}