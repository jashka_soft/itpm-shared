import {} from '@types/dhtmlxgantt';
import { Task } from './models/task';

function weekend(date: Date) {
  if (date.getDay() === 0 || date.getDay() === 6) {
    return 'weekend';
  }
}

export const GanttConfiguration = (gantt: GanttStatic) => {
  (gantt as any).form_blocks['checkbox'] = {
    render: (config: any) => `<input type='checkbox' value='true' name='${config.name}'>`,
    set_value: (node: any, value: any, ev: any, config: any) => node.checked = !!value,
    get_value: (node: any, ev: any, config: any) => !!node.checked,
    focus: (node: any) => {
    }
  };

// gantt.config.scale_height = 22 * 3;
// gantt.config.highlight_critical_path = true;
  (gantt as GanttStatic).config.columns = [
    // {name: 'wbs', label: 'WBS', width: 40, template: gantt.getWBSCode, 'resize': true},

    {
      name: 'text',
      label: 'Task name',
      tree: true,
      width: 170,
      resize: true,
      min_width: 10
    },
    {
      name: 'progress',
      label: 'Status',
      template: (obj: any) => Math.round(obj.progress * 100) + '%',
      align: 'center',
      width: 60,
      resize: true,
    },
    {
      name: 'checked',
      label: 'Checked',
      resize: true,
      template: (task: any) => task.open ? 'open' : 'closed',
      width: 80,
      align: 'center',
    },
    {
      name: 'priority',
      label: 'Priority',
      template: (obj: any) => gantt.getLabel('priority', obj.priority),
      resize: true,
      align: 'center',
      width: 65
    },
    {
      name: 'start_date',
      label: 'Start',
      align: 'center',
      width: 90,
      'resize': true
    },
    {
      name: 'end_date',
      label: 'End',
      align: 'center',
      width: 90,
      'resize': true
    },
    {
      name: 'duration',
      align: 'center',
      width: 80,
      'resize': true
    },
    {
      name: 'add',
      width: 40
    }
  ];
  gantt.config.details_on_create = true;
  gantt.config.order_branch = true;
  gantt.config.order_branch_free = true;

  gantt.config.grid_resize = true;
  gantt.config.grid_width = 390;

  (gantt.locale.labels as any)['section_priority'] = 'Priority';
  (gantt.locale.labels as any)['section_check'] = 'Open';

  gantt.config.lightbox.sections = [
    {
      name: 'description',
      height: 38,
      map_to: 'text',
      type: 'textarea',
      focus: true
    },
    {
      name: 'priority',
      height: 22,
      map_to: 'priority',
      type: 'select',
      options: [
        {key: '1', label: 'Low'},
        {key: '0', label: 'Normal'},
        {key: '2', label: 'High'}
      ]
    },
    {
      name: 'check',
      type: 'checkbox',
      map_to: 'open'
    },
    {
      name: 'time',
      type: 'duration',
      map_to: 'auto',
      time_format: ['%d', '%m', '%Y', '%H:%i']
    }
  ];

  gantt.templates.scale_cell_class = (date: any) => <string>weekend(date);
  gantt.templates.task_cell_class = (item: Date, date: Date) => <string>weekend(date);
  gantt.templates.task_class = (start: Date, end: Date, task: Task) => task.open ? 'gantt-task-open' : '';

  return gantt;
};