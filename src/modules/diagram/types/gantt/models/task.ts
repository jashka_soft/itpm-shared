export class Task {
  id: number;
  start_date: string | Date;
  end_date: string | Date;
  text: string;
  progress: number;
  duration: number;
  parent: number;
  open: boolean;
}