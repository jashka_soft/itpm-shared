import { ElementRef } from '@angular/core';

import {} from '@types/dhtmlxgantt';

import { Diagram } from '../diagram';
import { Task } from './models/task';
import { Link } from './models/link';
import { TypeExport } from '../../diagram.interface';
import { DiagramConfig } from '../../instances';
import { Schema } from '../../../itpm/models/schema';
import { Zoom } from './modules/zoom';
import { GanttConfiguration } from './gantt-configuration';
import { ControlPanel } from './modules/control-panel';
import { ColumnToggleMenu } from './modules/column-toggle-menu';
import { GanttSchema } from '../../../itpm/components/schema/schema';

export class Gantt extends Diagram {

  private gantt: GanttStatic;
  private tasks: Task[] = [];
  private links: Link[] = [];
  private controls: ControlPanel;
  private ganttUrl: string = 'assets/gantt/dhtmlxgantt.js';
  private apiUrl: string = 'http://export.dhtmlx.com/gantt/api.js';

  constructor(diagramContainer: ElementRef, config: DiagramConfig) {
    super(diagramContainer, config);
  }

  public export(type: TypeExport, schema: Schema) {
    this.loadApi(this.apiUrl, () => {
      super.export(type, schema);
    });
  }

  public initDiagram(schema: Schema) {
    this.loadApi(this.ganttUrl, () => {
      this.init(schema);
    });
  }

  public getDiagram(): any {
    return this.gantt;
  }

  protected getSourceModel(): any {
    return {
      tasks: this.tasks,
      links: this.links,
    };
  }

  protected addExampleData(): any {}

  protected print() {

  }

  protected init(schema: GanttSchema) {
    this.gantt = (window as any)['gantt'];
    this.gantt = GanttConfiguration(this.gantt);

    const zoom = new Zoom(this.gantt);
    zoom.setZoom(2);

    this.controls = new ControlPanel(this.gantt, this.div, zoom);
    const panel = this.controls.getPanel();
    const ganttDiv = document.createElement('div');

    ganttDiv.style.width = '100%';
    ganttDiv.style.height = '100%';

    this.div.appendChild(panel);
    this.div.appendChild(ganttDiv);

    this.controls.initEvents();

    const { data, links } = this.makeDefaultData(schema);

    this.tasks = data.map((task: Task) => {
      task.start_date = new Date(Date.parse(<string>task.start_date));
      task.end_date = new Date(Date.parse(<string>task.end_date));

      return task;
    });
    this.links = links;

    this.rbac.onRead(() => {
      this.gantt.config.readonly = true;
    }, () => {
      this.initEvents();
    });

    this.gantt.init(ganttDiv);
    this.gantt.parse({data, links});

    new ColumnToggleMenu(this.gantt);

    this.updateModelInStorage();
  }

  protected downloadImage(schema: Schema) {
    const name = this.makeExportName(schema, 'png');
    (this.gantt as any).exportToPNG({ name });
  }

  protected downloadPDF(schema: Schema) {
    const name = this.makeExportName(schema, 'pdf');
    (this.gantt as any).exportToPDF({ name });
  }

  private makeExportName(schema: Schema, extension: string): string {
    const name = schema.name ? schema.name : '';
    const date = new Date().toDateString();
    return `${name} - ${schema.type} - ${date}.${extension}`;
  }

  private makeDefaultData(schema: GanttSchema): any {
    let data: Task[] = [],
      links: Link[] = [];

    if (schema.diagram) {
      data = schema.diagram['tasks'] || [];
      links = schema.diagram['links'] || [];
    }

    return { data, links };
  }

  private initEvents() {

    this.gantt.attachEvent('onAfterTaskAdd', (id: number, task: Task) => {
      task.duration = 0;
      task.progress = 0;
      this.tasks.push(task);
      if (task.id !== id) {
        this.gantt.changeTaskId(id, task.id);
      }
      this.updateModelInStorage();
    });

    this.gantt.attachEvent('onAfterTaskUpdate', (id: number, task: Task) => {
      const index = this.findIndexById<Task>(this.tasks, id);
      if (typeof index === 'number') {
        this.tasks[index] = task;
      }
      this.updateModelInStorage();
    });

    this.gantt.attachEvent('onAfterTaskDelete', (id: number) => {
      const index = this.findIndexById<Task>(this.tasks, id);
      if (typeof index === 'number') {
        this.tasks.splice(index, 1);
      }
      this.updateModelInStorage();
    });

    this.gantt.attachEvent('onAfterLinkAdd', (id: number, link: Link) => {
      this.links.push(link);
      if (link.id !== id) {
        this.gantt.changeTaskId(id, link.id);
      }
      this.updateModelInStorage();
    });

    this.gantt.attachEvent('onAfterLinkUpdate', (id: number, link: Link) => {
      const index = this.findIndexById<Link>(this.links, id);
      if (typeof index === 'number') {
        this.links[index] = link;
      }
      this.updateModelInStorage();
    });

    this.gantt.attachEvent('onAfterLinkDelete', (id: number) => {
      const index = this.findIndexById<Link>(this.links, id);
      if (typeof index === 'number') {
        this.links.splice(index, 1);
      }
      this.updateModelInStorage();
    });
  }

  private findIndexById<T>(data: T[], value: number, key: string = 'id'): number {
    return data.findIndex((type: T) => type[key] === value);
  }

  private updateModelInStorage() {
    this.config.schema.diagram = this.getSourceModel();
    this.updateModel(this.config.schema);
    this.controls.refreshUndoBtns();
  }

  private loadApi(url: string, fn: Function) {
    if (document.querySelector(`script[src='${url}']`)) {
      fn.bind(this)();
    } else {
      const script = document.createElement('script');
      script.src = url;
      script.async = true;
      script.onload = () => {
        fn.bind(this)();
      };
      document.head.appendChild(script);
    }
  }
}