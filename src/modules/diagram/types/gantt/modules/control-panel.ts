import {} from '@types/dhtmlxgantt';

import { Task } from '../models/task';
import { Zoom } from './zoom';
import { ZoomToFit } from './zoom-to-fit';

export class ControlPanel {
  private template: string = '';
  private panelClass: string = '.gantt-control-panel';
  private panelElement: Element;

  private zoomToFit: ZoomToFit;

  public constructor(private gantt: GanttStatic,
                     private div: any,
                     private zoom: Zoom) {
    this.zoomToFit = new ZoomToFit(this.gantt);
  }

  public getPanel(): Node | null {
    this.template = this.makePanelTemplate();

    return this.parseTemplate();
  }

  public initEvents() {
    this.panelElement = <Element>document.querySelector(this.panelClass);
    this.panelElement.addEventListener('click', (event: any) => {
      let target = event.target || event.srcElement;
      while (!target.hasAttribute('data-action') && target !== document.body) {
        target = target.parentNode;
      }

      if (target && target.hasAttribute('data-action')) {
        const action = target.getAttribute('data-action');
        const self = this as any;
        const methodName = `${action}Action`;
        const method = self[methodName];
        if (typeof method === 'function') {
          method.bind(this)(event, action);
        }
      }
    });
  }

  public refreshUndoBtns() {
    if (!this.gantt.getUndoStack().length) {
      this.disableButton("undo");
    } else {
      this.enableButton("undo");
    }

    if (!this.gantt.getRedoStack().length) {
      this.disableButton("redo");
    } else {
      this.enableButton("redo");
    }

  }

  private collapseAllAction() {
    this.gantt.eachTask((task: Task | any) => {
      task.$open = false;
    });
    this.gantt.render();
  }

  private expandAllAction() {
    this.gantt.eachTask((task: Task | any) => {
      task.$open = true;
    });
    this.gantt.render();
  }

  private toggleCriticalPathAction(event: any, action: string) {
    this.gantt.config.highlight_critical_path = !this.gantt.config.highlight_critical_path;
    this.gantt.config.highlight_critical_path
      ? this.highlightButton(action)
      : this.unhighlightButton(action);
    this.gantt.render();
  }

  private zoomInAction() {
    this.zoomToFit.disable();
    this.zoom.zoomIn();
    this.refreshZoomBtns();
    this.toggleZoomToFitBtn();
  }

  private zoomOutAction() {
    this.zoomToFit.disable();
    this.zoom.zoomOut();
    this.refreshZoomBtns();
    this.toggleZoomToFitBtn();
  }

  private zoomToFitAction() {
    this.zoom.deactivate();
    this.zoomToFit.toggle();
    this.toggleZoomToFitBtn();
    this.refreshZoomBtns();
  }

  private undoAction() {
    this.gantt.undo();
    this.refreshUndoBtns();
  }

  private redoAction() {
    this.gantt.redo();
    this.refreshUndoBtns();
  }

  private parseTemplate(): Node | null {
    const parser = new DOMParser();
    const doc = parser.parseFromString(this.template, 'text/html');
    return doc.body.firstChild;
  }

  private getButton(name: string): Element {
    return <Element>document.querySelector(`.gantt-controls [data-action='${name}']`);
  }

  private highlightButton(name: string) {
    this.getButton(name).classList.add('menu-item-active');
  }

  private unhighlightButton(name: string) {
    this.getButton(name).classList.remove('menu-item-active');
  }

  private disableButton(name: string) {
    this.getButton(name).classList.add('menu-item-disabled');
  }

  private enableButton(name: string) {
    this.getButton(name).classList.remove('menu-item-disabled');
  }

  private refreshZoomBtns() {
    const zoom = this.zoom;
    if (zoom.canZoomIn()) {
      this.enableButton("zoomIn");
    } else {
      this.disableButton("zoomIn");
    }
    if (zoom.canZoomOut()) {
      this.enableButton("zoomOut");
    } else {
      this.disableButton("zoomOut");
    }
  }

  private toggleZoomToFitBtn() {
    if (this.zoomToFit.isEnabled()) {
      this.highlightButton("zoomToFit");
    } else {
      this.unhighlightButton("zoomToFit");
    }
  }

  private makeMenuItem({action, icon, text, classes, textClasses}: any) {
    return `<li class='gantt-menu-item ${classes ? classes : ''}'>
    <a data-action='${action}' class='${textClasses ? textClasses : ''}'>
        ${icon ? `<img src='assets/gantt/imgs/${icon}.png'>` : ''}
    ${text}</a>
</li>`;
  }

  private makePriorityInput({name, value, checked}: any) {
    return `<li class="gantt-menu-item">
    <label>
        <a class="priority-item">
            <input
                type="radio"
                value="${value}"
                ${checked ? 'checked="true"' : ''}
                name="priority">
            <span>${name}</span>
        </a>
    </label>
</li>`;
  }

  private makePanelTemplate(): string {
    return `<div class='header gantt-demo-header gantt-control-panel'>
    <ul class='gantt-controls'>
${this.makeMenuItem({
      action: 'collapseAll',
      icon: 'ic_collapse_all_24',
      text: 'Collapse All',
    })}

${this.makeMenuItem({
      action: 'expandAll',
      icon: 'ic_expand_all_24',
      text: 'Expand All',
      classes: 'gantt-menu-item-last'
    })}

${this.makeMenuItem({
      action: 'undo',
      icon: 'ic_undo_24',
      text: 'Undo',
      textClasses: 'menu-item-disabled'
    })}

${this.makeMenuItem({
      action: 'redo',
      icon: 'ic_redo_24',
      text: 'Redo',
      classes: 'gantt-menu-item-last',
      textClasses: 'menu-item-disabled',
    })}

${this.makeMenuItem({
      action: 'toggleCriticalPath',
      icon: 'ic_critical_path_24',
      text: 'Critical Path',
    })}
   
<!--${this.makeMenuItem({
      action: 'toggleFilter',
      text: 'Filter',
      classes: 'gantt-menu-item-right',
    })}-->
    
${this.makeMenuItem({
      action: 'zoomToFit',
      icon: 'ic_zoom_to_fit_24',
      text: 'Zoom to Fit',
      classes: 'gantt-menu-item-right gantt-menu-item-last',
    })}       

${this.makeMenuItem({
      action: 'zoomOut',
      icon: 'ic_zoom_out',
      text: 'Zoom Out',
      classes: 'gantt-menu-item-right',
    })}
          
${this.makeMenuItem({
      action: 'zoomIn',
      icon: 'ic_zoom_in',
      text: 'Zoom In',
      classes: 'gantt-menu-item-right',
    })}

    </ul>
    <!--<div class="priority-filter" style="display: none;">
            <strong> Filtering: &nbsp; </strong>
            <ul class="gantt-controls priority-list">
                ${ this.makePriorityInput({name: 'All', value: '', checked: true}) }
                ${ this.makePriorityInput({name: 'Low', value: 0,}) }
                ${ this.makePriorityInput({name: 'Normal', value: 1,}) }
                ${ this.makePriorityInput({name: 'High', value: 2,}) }
            </ul>
    </div>-->
</div>`;
  }
}