import {} from '@types/dhtmlxgantt';

export class Zoom {
  private configs: any = {};
  private isActive: boolean = true;
  private current: number = 0;

  public constructor(private gantt: GanttStatic) {
    this.configs = this.makeConfigs();
  }

  public deactivate() {
    this.isActive = false;
  }

  public setZoom(level: number) {
    this.isActive = true;
    this.current = level;

    this.setScaleConfig(this.current);
    this.refresh();
  }

  public zoomOut() {
    if (this.canZoomOut()) {
      this.isActive = true;
      this.current = (this.current + 1);
      if (!this.configs[this.current])
        this.current = 6;

      this.setScaleConfig(this.current);
      this.refresh();
    }
  }

  public zoomIn() {
    if (this.canZoomIn()) {
      this.isActive = true;
      this.current = (this.current - 1);
      if (!this.configs[this.current])
        this.current = 1;
      this.setScaleConfig(this.current);
      this.refresh();
    }
  }

  public canZoomOut() {
    return !this.isActive || !!(this.configs[this.current + 1]);
  }

  public canZoomIn() {
    return !this.isActive || !!(this.configs[this.current - 1]);
  }

  private setScaleConfig(config: any) {
    this.configs[config]();
  }

  private refresh() {
    if ((this.gantt as any).$container)
      this.gantt.render();
  }

  private makeConfigs() {
    return {
      1: () => {
        this.gantt.config.scale_unit = 'day';
        this.gantt.config.step = 1;
        this.gantt.config.date_scale = '%d %M';
        this.gantt.config.min_column_width = 30;
        this.gantt.config.subscales = [
          {unit: 'hour', step: 1, date: '%h'}
        ];
        this.gantt.config.round_dnd_dates = true;

        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';
      },
      2: () => {

        this.gantt.config.scale_unit = 'week';
        this.gantt.config.date_scale = '%W';
        this.gantt.config.step = 1;
        this.gantt.templates.date_scale = () => '';
        this.gantt.config.min_column_width = 60;
        this.gantt.config.subscales = [
          {unit: 'month', step: 1, date: '%M'},
          {unit: 'day', step: 1, date: '%D'}
        ];
        this.gantt.config.round_dnd_dates = true;
        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';
      },
      3: () => {
        this.gantt.config.scale_unit = 'year';
        this.gantt.config.date_scale = '%Y';
        this.gantt.config.min_column_width = 60;
        this.gantt.config.subscales = [
          {unit: 'month', step: 1, date: '%M'},
          {unit: 'week', step: 1, date: '%W'}
        ];
        this.gantt.config.round_dnd_dates = false;
        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';
      },
      4: () => {
        this.gantt.config.scale_unit = 'year';
        this.gantt.config.step = 1;
        this.gantt.config.date_scale = '%Y';
        this.gantt.config.min_column_width = 50;
        this.gantt.config.round_dnd_dates = false;
        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';


        this.gantt.config.subscales = [
          {unit: 'month', step: 1, date: '%M'}
        ];
      },
      5: () => {
        this.gantt.config.scale_unit = 'year';
        this.gantt.config.step = 1;
        this.gantt.config.date_scale = '%Y';
        this.gantt.config.min_column_width = 50;
        this.gantt.config.round_dnd_dates = false;
        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';


        function quarterLabel(date: any) {
          let month = date.getMonth();
          let q_num;

          if (month >= 9) {
            q_num = 4;
          } else if (month >= 6) {
            q_num = 3;
          } else if (month >= 3) {
            q_num = 2;
          } else {
            q_num = 1;
          }

          return 'Q' + q_num;
        }

        this.gantt.config.subscales = [
          {unit: 'quarter', step: 1, template: quarterLabel},
          {unit: 'month', step: 1, date: '%M'}
        ];
      },
      6: () => {
        this.gantt.config.scale_unit = 'year';
        this.gantt.config.round_dnd_dates = false;
        this.gantt.config.step = 1;
        this.gantt.config.date_scale = '%Y';
        this.gantt.config.min_column_width = 50;

        this.gantt.config.scale_height = 60;
        this.gantt.templates.date_scale = () => '';

        this.gantt.config.subscales = [];
      }
    };
  }
}