import {} from '@types/dhtmlxgantt';

export class ZoomToFit {
  private enabled: boolean = false;
  private cachedSettings: any = {};
  private scaleConfigs = [
    // minutes
    {
      unit: 'minute',
      step: 1,
      scale_unit: 'hour',
      date_scale: '%H',
      subscales: [
        {
          unit: 'minute',
          step: 1,
          date: '%H:%i'
        }
      ]
    },
    // hours
    {
      unit: 'hour',
      step: 1,
      scale_unit: 'day',
      date_scale: '%j %M',
      subscales: [
        {
          unit: 'hour',
          step: 1,
          date: '%H:%i'
        }
      ]
    },
    // days
    {
      unit: 'day',
      step: 1,
      scale_unit: 'month',
      date_scale: '%F',
      subscales: [
        {
          unit: 'day',
          step: 1,
          date: '%j'
        }
      ]
    },
    // weeks
    {
      unit: 'week',
      step: 1,
      scale_unit: 'month',
      date_scale: '%F',
      subscales: [
        {
          unit: 'week',
          step: 1,
          template: (date: any) => {
            return this.dateTemplate({date, format: '%d %M', value: 1, type: 'week'});
          }
        }
      ]
    },
    // months
    {
      unit: 'month',
      step: 1,
      scale_unit: 'year',
      date_scale: '%Y',
      subscales: [
        {
          unit: 'month',
          step: 1,
          date: '%M'
        }
      ]
    },
    // quarters
    {
      unit: 'month',
      step: 3,
      scale_unit: 'year',
      date_scale: '%Y',
      subscales: [
        {
          unit: 'month',
          step: 3,
          template: (date: any) => {
            return this.dateTemplate({date, format: '%M', value: 3, type: 'month'});
          }
        }
      ]
    },
    // years
    {
      unit: 'year',
      step: 1,
      scale_unit: 'year',
      date_scale: '%Y',
      subscales: [
        {
          unit: 'year',
          step: 5,
          template: (date: any) => {
            return this.dateTemplate({date, format: '%Y', value: 5, type: 'year'});
          }
        }
      ]
    },
    // decades
    {
      unit: 'year',
      step: 10,
      scale_unit: 'year',
      template: (date: any) => {
        return this.dateTemplate({date, format: '%Y', value: 10, type: 'year'});
      },
      subscales: [
        {
          unit: 'year',
          step: 100,
          template: (date: any) => {
            return this.dateTemplate({date, format: '%Y', value: 100, type: 'year'});
          }
        }
      ]
    }
  ];

  public constructor(private gantt: GanttStatic) {
  }

  public enable() {
    if (!this.enabled) {
      this.enabled = true;
      this.saveConfig();
      this.zoomToFit();
      this.gantt.render();
    }
  }

  public isEnabled() {
    return this.enabled;
  }

  public toggle() {
    if (this.isEnabled()) {
      this.disable();
    } else {
      this.enable();
    }
  }

  public disable() {
    if (this.enabled) {
      this.enabled = false;
      this.restoreConfig();
      this.gantt.render();
    }
  }


  private saveConfig() {
    let config = this.gantt.config;
    this.cachedSettings = {};
    this.cachedSettings.scale_unit = config.scale_unit;
    this.cachedSettings.date_scale = config.date_scale;
    this.cachedSettings.step = config.step;
    this.cachedSettings.subscales = config.subscales;
    this.cachedSettings.template = gantt.templates.date_scale;
    this.cachedSettings.start_date = config.start_date;
    this.cachedSettings.end_date = config.end_date;
  }

  private restoreConfig() {
    this.applyConfig(this.cachedSettings);
  }

  private applyConfig(config: any, dates?: any) {
    this.gantt.config.scale_unit = config.scale_unit;
    if (config.date_scale) {
      this.gantt.config.date_scale = config.date_scale;
      this.gantt.templates.date_scale = () => '';
    }
    else {
      this.gantt.templates.date_scale = config.template;
    }

    this.gantt.config.step = config.step;
    this.gantt.config.subscales = config.subscales;

    if (dates) {
      this.gantt.config.start_date = this.gantt.date.add(dates.start_date, -1, config.unit);
      this.gantt.config.end_date = this.gantt.date.add((this.gantt.date as any)[config.unit + '_start'](dates.end_date), 2, config.unit);
    } else {
      (this.gantt.config as any).start_date = (this.gantt.config as any).end_date = null;
    }
  }

  private zoomToFit() {
    let project = this.gantt.getSubtaskDates(),
      areaWidth = (this.gantt as any).$task.offsetWidth,
      i = 0;

    for (; i < this.scaleConfigs.length; i++) {
      let columnCount = this.getUnitsBetween(project.start_date, project.end_date, this.scaleConfigs[i].unit, this.scaleConfigs[i].step);
      if ((columnCount + 2) * this.gantt.config.min_column_width <= areaWidth) {
        break;
      }

      if (i === this.scaleConfigs.length) {
        i--;
      }

      this.applyConfig(this.scaleConfigs[i], project);
    }
  }

  // get number of columns in timeline
  private getUnitsBetween(from: any, to: any, unit: string, step: number) {
    let start = new Date(from),
      end = new Date(to);
    let units = 0;
    while (start.valueOf() < end.valueOf()) {
      units++;
      start = gantt.date.add(start, step, unit);
    }
    return units;
  }

  private dateTemplate({date, format, value, type}: any) {
    let dateToStr = gantt.date.date_to_str(format);
    let endDate = gantt.date.add(gantt.date.add(date, value, type), -1, 'day');
    return `${dateToStr(date)} - ${dateToStr(endDate)}`;
  }

}