import {} from '@types/dhtmlxgantt';

declare const dhtmlXMenuObject: any;

export class ColumnToggleMenu {
  private menu: any;

  public constructor(private gantt: GanttStatic) {
    this.menu = new dhtmlXMenuObject();
    this.init();
  }

  private init() {
    this.menu.renderAsContextMenu();

    this.gantt.attachEvent('onContextMenu', this.onContextMenu.bind(this));

    this.menu.attachEvent('onClick', this.onClick.bind(this));
  }

  private onContextMenu(taskId: any, linkId: any, event: any) {
    let x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft,
      y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;

    let target = (event.target || event.srcElement);
    let column_id = target.getAttribute('column_id');
    this.menu.clearAll();

    this.addColumnsConfig();
    if (column_id) {
      this.addColumnToggle(column_id);
    }

    this.menu.showContextMenu(x, y);
    return false;
  }

  private addColumnToggle(column_name: any) {
    let column = this.gantt.getGridColumn(column_name);
    let label = this.getColumnLabel(column);

    //add prefix to distinguish from the same item in 'show columns' menu
    let item_id = 'toggle#' + column_name;
    this.menu.addNewChild(null, -1, item_id, `Hide '${label}'`, false);
    this.menu.addNewSeparator(item_id);
  }

  private addColumnsConfig() {
    this.menu.addNewChild(null, -1, 'show_columns', 'Show columns:', false);
    let columns = this.gantt.config.columns;

    for (let i = 0; i < columns.length; i++) {
      let checked = (!columns[i].hide),
        itemLabel = this.getColumnLabel(columns[i]);
      this.menu.addCheckbox('child', 'show_columns', i, columns[i].name, itemLabel, checked);
    }
  }

  private getColumnLabel(column: any) {
    if (column === null)
      return '';

    let locale: any = this.gantt.locale.labels;
    let text = column.label !== undefined ? column.label : locale['column_' + column.name];

    text = text || column.name;
    return text;
  }

  private onClick(id: any, zoneId: any, cas: any) {
    let parts = (id + '').split('#');
    let is_toggle = parts[0] === 'toggle',
      column_id = parts[1] || id;

    let column = this.gantt.getGridColumn(column_id);

    if (column) {
      let visible = !is_toggle ? this.menu.getCheckboxState(id) : false;
      column.hide = !visible;
      this.gantt.render();
    }
    return true;
  }
}