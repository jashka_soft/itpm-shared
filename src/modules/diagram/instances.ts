import { ElementRef } from "@angular/core";

import { WBS } from "./types/gojs/wbs/wbs";
import { PERT } from "./types/gojs/pert/pert";
import { Diagram } from "./types/diagram";
import { Gantt } from "./types/gantt/gantt";
import { Schema, WBSSchema, GoSchema, GanttSchema } from '../itpm/components/schema/schema';
import { Permissions } from './security/permission/permissions';
import { ConfigSchemaInterface } from '../itpm/components/schema/config-schema.interface';

export interface DiagramConfig {
  permissions: Permissions,
  data?: any;
  controlsRef: ElementRef;
  schema: Schema;
}

export interface DiagramInstanceConfig {
  value: string;
  'class': any;
  name: string;
  schemaClass: typeof Schema;
  config?: DiagramConfig
}

export const instances: DiagramInstanceConfig[] = [
  {
    value: 'WBSOBS',
    'class': WBS,
    name: 'WBS/OBS',
    schemaClass: WBSSchema,
  },
  {
    value: 'PERT',
    'class': PERT,
    name: 'PERT',
    schemaClass: GoSchema
  },
  {
    value: 'Gantt',
    'class': Gantt,
    name: 'Gantt',
    schemaClass: GanttSchema
  }

];

// diagram: any, div: ElementRef, config: DiagramConfig

export function DiagramFactory(schema: Schema,
                               config: ConfigSchemaInterface,
                               diagram: ElementRef,
                               control: ElementRef): Diagram {
  const diagramInstanceData = instances.find((element, index) => schema.type === element.value);

  if (!diagramInstanceData) {
    throw new Error(`Diagram by type '${schema.type}' not found`);
  }

  const diagramClass = diagramInstanceData['class'];
  diagramInstanceData.config = {
    permissions: config.permission,
    data: {
      role: config.role
    },
    controlsRef: control,
    schema: schema
  };

  return new diagramClass(diagram, diagramInstanceData.config);
}